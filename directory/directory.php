<?php
require 'class-education-childcare.php';
require 'class-fun.php';

require 'class-parties-event.php';
require 'class-pregnancy-baby.php';

//class instance create
new Education_Childcare();
new Fun_Around_Town();
new Parties_Events();
new Pregnancy_Baby();
add_filter('ever_import_insert_post', 'post_city_ever_import_insert_post');

function post_city_ever_import_insert_post($post)
{
	if (!array_key_exists('_sponsored', $post['meta_input'])) {
		$post['meta_input']['_sponsored'] = '0';
	}

	if (!array_key_exists('_top_listing', $post['meta_input'])) {
		$post['meta_input']['_top_listing'] = '0';
	}

	return $post;
}

