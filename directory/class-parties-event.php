<?php

class Parties_Events extends \Pluginever\WebDirectoryPro\Listing\Listing
{
	protected $listing_type = 'parties_events';

	/**
	 * Parties_Events constructor.
	 */
	public function hooks()
	{
//		add_action( 'wdp_single_before_content_' . $this->listing_type, array( $this, 'show_taxonomies_in_main_area' ), 10 );
//		add_action( 'wdp_single_right_left_' . $this->listing_type, array( $this, 'show_claim_listing_button' ) );
//		add_action( 'wdp_single_right_left_' . $this->listing_type, array( $this, 'show_taxonomies_in_sidebar' ) );
//
//		add_filter( 'wdp_settings_fields', array( $this, 'add_settings_fields' ) );
//
//		add_filter( 'wpd_get_listing_results_doctor', array( $this, 'add_specialities' ) );
//		add_action( 'wdp_listing_loop_after_title_doctor', array( $this, 'print_listing_meta' ) );
//
//		add_action( 'wdp_listing_meta_details_top', array( $this, 'add_meta_fields' ) );
//
//		add_filter( 'wdp_post_metas_' . $this->listing_type, array( $this, 'add_post_meta' ) );
	}

	/**
	 * set listing type
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public function set_listing_type()
	{
		return $this->listing_type;
	}

	public function set_post_props()
	{
		$args = array(
			'label' => 'Parties & Events',
		);

		return $args;
	}

	public function set_taxonomies()
	{
		$taxonomies = array(
			'parties_events_category' => array(
				'label'        => __('Category', 'web-directory-pro'),
				'hierarchical' => true,
			)

		);

		return $taxonomies;
	}

	public function set_filter_taxonomies()
	{
		$filters = array(
			array(
				'taxonomy' => 'parties_events_category',
				'orderby'  => 'name',
				'order'    => 'DESC',
				'label'    => 'Category',
			)
		);

		return $filters;
	}
}
