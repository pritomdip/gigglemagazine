<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package themeplate
 */
?>
<div class="footer-area" id="wrapper-footer">

	<div class="footer-top-block">
		<div class="follow-us-area">
			<div class="item">
				<span class="text">follow us</span>
			</div>
			<div class="item">
				<!--<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fgigglemagazine&width=93&layout=button_count&action=like&size=large&show_faces=true&share=false&height=21&appId=696074373923706" width="93" height="40" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>-->
				<a href="https://www.facebook.com/gigglemagazine" target="_blank">
					<img src="<?php bloginfo('template_directory') ?>/assets/images/facebook.png" alt="image">
					<!-- <span>10000</span> -->
				</a>
			</div>
			<div class="item">
				<a href="https://www.pinterest.com/gigglemagazine/?d" target="_blank">
					<img src="<?php bloginfo('template_directory') ?>/assets/images/pinterest.png" alt="image">
					<!-- <span>10000</span> -->
				</a>
			</div>
			<div class="item">
				<a href="https://twitter.com/GiggleMagazine" target="_blank">
					<img src="<?php bloginfo('template_directory') ?>/assets/images/twitter.png" alt="image">
					<!-- <span>10000</span> -->
				</a>
			</div>
			<div class="item">
				<a href="https://www.instagram.com/gigglemagazine/" target="_blank">
					<img src="<?php bloginfo('template_directory') ?>/assets/images/instagram.png" alt="image">
					<!-- <span>10000</span> -->
				</a>
			</div>
		</div>
	</div><!--footer-top-block-->

	<div class="footer-bottom-block">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="footer-message-area">
						<div class="footer-message-top">
							<p>
								Great magazine for Gainesville. I love reading the articles and now that I am about to
								become a
								mom. This magazine holds a new place in my heart. Keep up the great work!
							</p>
						</div>
						<div class="footer-message-bottom">
							<p>Stephanie G</p>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="footer-right-area">
						<div class="row">
							<div class="col-md-4">
								<?php dynamic_sidebar('footer-1'); ?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer-2'); ?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer-3'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- container end -->
	</div><!--footer-bottom-block-->

	<div class="site-footer-area">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<footer id="colophon" class="site-footer" role="contentinfo">
						<?php dynamic_sidebar('footer-site-info'); ?>
					</footer><!-- #colophon -->
				</div><!--col end -->
			</div><!-- row end -->
		</div>
	</div><!--site-footer-area-->


</div><!-- wrapper end -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>

