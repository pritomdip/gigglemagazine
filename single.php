<?php
/**
 * The template for displaying all single posts.
 *
 * @package themeplate
 */

get_header(); ?>
<div class="wrapper" id="single-wrapper">
	<div id="content" class="container">
		<div class="row">

			<div id="primary" class="col-md-9 content-area">
				<main id="main" class="site-main" role="main">
					<?php while (have_posts()) : the_post();
						get_template_part('loop-templates/content', 'single');
						if (class_exists('CoAuthors_Plus')) {
							global $post;
							$co_authors = get_coauthors($post->ID);
							foreach ($co_authors as $key => $author) {
								get_template_part('loop-templates/author', 'bio');

							}
						}
						//social-share
						get_template_part('template-parts/social-share');
					endwhile; ?>

					<!--read comments-btn-->
					<div class="read-comments-area hide">
						<a href="#" class="btn read-comment-btn">READ COMMENTS ADD COMMENTS MODULE TK</a>
					</div>

					<!--related stories-->
					<?php get_template_part('template-parts/related-post'); ?>

					<!--add-banner-->
					<div class="add-banner">
						<a href="#">
							<img class="image-responsive"
								 src="<?php bloginfo('template_directory') ?>/assets/images/banner3.png"
								 alt="banner-image">
						</a>
					</div>


				</main><!-- #main -->
			</div><!-- #primary -->

			<div id="secondary" class="col-md-3">
				<div class="sidebar single-sidebar">
					<?php dynamic_sidebar('single-page-sidebar-1'); ?>
				</div><!--single-sidebar-->
			</div>

		</div><!-- .row -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
