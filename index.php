<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package themeplate
 */

$excluded_posts = [];
global $wp_query;
$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
get_header();
?>

<div class="wrapper" id="archive-wrapper">
	<div id="content" class="category-archive">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">


				<div class="container">
					<div class="feature-slider-inner owl-carousel">
						<?php
						$post_args     = array(
							'posts_per_page' => 6,
							'post_type'      => 'post',
							'post_status'    => 'publish',
							'meta_query'     => array(
								array(
									'key'     => '_feature_post',
									'value'   => 'on',
									'compare' => '=',
								)
							),
							'exclude'        => $excluded_posts
						);
						$feature_posts = get_posts($post_args);
						if (!empty($feature_posts)) {
							$featured_post_ids = wp_list_pluck($feature_posts, 'ID');
							$excluded_posts    = array_merge($excluded_posts, $featured_post_ids);
							foreach ($feature_posts as $post) {
								setup_postdata($post);
								get_template_part('loop-templates/slider-feature-post');
							}
							wp_reset_postdata();
						} ?>

					</div><!--feature-slider-inner-->
				</div><!--container-->


				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<!--hero-banner-->
							<div class="hero-banner">
								<a href="#">
									<img
										src="<?php bloginfo('template_directory') ?>/assets/images/hero-banner-800X200.png"
										alt="banner-image">
								</a>
							</div>
							<!--hero-banner end-->
							<?php
							$blog_posts = get_posts(
								array(
									'post_type'      => 'post',
									'posts_per_page' => 20,
									'post_status'    => 'publish',
									'exclude'        => $excluded_posts,
								)
							);
							if ($blog_posts) {
								?>
								<div class="blog-post-area section-padding3">
									<div class="row height-auto">
										<?php
										$blog_post_ids  = wp_list_pluck($blog_posts, 'ID');
										$excluded_posts = array_merge($excluded_posts, $blog_post_ids);
										foreach ($blog_posts as $post) {
											setup_postdata($post);
											get_template_part('loop-templates/content-blog');
										}
										wp_reset_postdata();
										?>
										<div class="custom-pagination text-center">
											<div class="pagination">
												<?php
												$big = 999999999;
												echo paginate_links(array(
													'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
													'format'    => '?paged=%#%',
													'current'   => max(1, $current_page),
													'total'     => $wp_query->max_num_pages,
													'prev_text' => '<span><i class="fa fa-angle-double-left"></i></span>',
													'next_text' => '<span><i class="fa fa-angle-double-right"></i></span>'
												));
												?>
											</div>
										</div>

									</div>
								</div>
							<?php } ?>
						</div><!--col-md-9-->

						<div class="col-md-3">
							<div class="sidebar category-sidebar1 p-b-30">
								<?php dynamic_sidebar('sidebar-1'); ?>
							</div><!--sidebar-->
						</div>

					</div><!--row-->
				</div><!--container-->

			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
