<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package themeplate
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">

	<!-- ******************* navigation area ******************* -->
	<div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">

		<div class="header-top-section clearfix">
			<div class="container">
				<div class="header-top-left">
					<div class="logo-area">
						<a href="<?php echo get_site_url() ?>">
							<img src="<?php bloginfo('template_directory') ?>/assets/images/giggle-magazine.png" alt="">
						</a>
					</div><!--logo-area-->

					<div class="top-menu-area">
						<ul class="top-menu-list">
							<li>
								<a href="#">happy family</a>
								<!--<ul class="sub-title">
									<li>Alachua County's Premier Family and Parenting Magazine</li>
								</ul>-->
							</li>
							<li>
								<a href="#">happy community</a>
								<ul class="sub-title">
									<li>Alachua County's Premier Family and Parenting Magazine</li>
								</ul>
							</li>
						</ul>
					</div><!--top-menu-area-->

				</div>

				<div class="header-top-right">
					<div class="header-top-right-inner">

						<div class="hamburger-area">
							<div class="hamburger-button">
								<img class="hamburger-icon"
									 src="<?php bloginfo('template_directory') ?>/assets/images/hamburgerblack.png"
									 alt="search-icon">
							</div>

							<div class="hamburger-menu">
								<span class="hamburger-menu-close"><i class="fa fa-close"></i> </span>
								<div class="hamburger-menu-body">
									<?php
									wp_nav_menu(array(
										'theme_location' => 'primary',
										'depth'          => 2, // 1 = no dropdowns, 2 = with dropdowns.
										'menu_class'     => 'navbar-nav mr-auto',
										'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
										'walker'         => new WP_Bootstrap_Navwalker(),
									));
									?>
								</div>
							</div>
							<div class="hamburger-full-body-overlay"></div>
						</div><!--hamburger-area-->


						<div class="social-link-area">
							<a href="https://www.facebook.com/gigglemagazine" target="_blank">
								<img src="<?php bloginfo('template_directory') ?>/assets/images/facebook.png"
									 alt="image">
							</a>
							<a href="https://www.pinterest.com/gigglemagazine/?d" target="_blank">
								<img src="<?php bloginfo('template_directory') ?>/assets/images/pinterest.png"
									 alt="image">
							</a>
							<a href="https://twitter.com/GiggleMagazine" target="_blank">
								<img src="<?php bloginfo('template_directory') ?>/assets/images/twitter.png"
									 alt="image">
							</a>
							<a href="https://www.instagram.com/gigglemagazine/" target="_blank">
								<img src="<?php bloginfo('template_directory') ?>/assets/images/instagram.png"
									 alt="image">
							</a>
						</div><!--social-link-area-->


						<div class="header-search-box">
							<form method="get" action="" role="search">
								<input type="text" name="s" class="search-field" placeholder="search">
								<button type="submit"><i class="fa fa-search"></i></button>
							</form>
						</div><!--header-search-box-->
					</div>

				</div>


			</div>
		</div>

		<nav class="site-navigation" itemscope="itemscope">
			<div class="navbar">

				<div class="container">

					<div class="row">

						<div class="col-xs-12">
							<!-- The WordPress Menu goes here -->
							<?php wp_nav_menu(
								array(
									'theme_location'  => 'primary',
									'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
									'menu_class'      => 'main-menu nav navbar-nav',
									'fallback_cb'     => '',
									'menu_id'         => 'main-menu',
									'walker'          => new wp_bootstrap_navwalker()
								)
							); ?>

						</div> <!-- .col-md-11 or col-md-12 end -->

					</div> <!-- .row end -->

				</div> <!-- .container -->

			</div><!-- .navbar -->

		</nav><!-- .site-navigation -->

	</div><!-- .wrapper-navbar end -->
