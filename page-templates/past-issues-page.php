<?php
/**
 * Template Name: Past Issues Page
 */
get_header(); ?>

<div class="wrapper" id="page-wrapper">
	<div id="content" class="container">
		<div class="row">
			<div id="primary" class="col-md-12 content-area">
				<main id="main" class="site-main" role="main">
					<?php
					$past_issues_post = get_posts(array(
						'posts_per_page' => -1,
						'post_type' => 'past_issues',
						'post_status' => 'publish',
						'orderby' => 'publish_date',
						'order' => 'DESC'
					));
					if (!empty($past_issues_post)) { ?>
						<div class="past-issues-area">
							<div class="row height-auto">
								<?php foreach ($past_issues_post
											   as $post) {
									setup_postdata($post);
									$website_url = esc_url(get_post_meta($post->ID, '_website_url', true));
									?>
									<div class="col-md-2">
										<div class="issues-item m-b-30">
											<a href="<?php echo $website_url; ?>">
												<?php if (has_post_thumbnail()) {
													echo get_the_post_thumbnail($post, '');
												} else { ?>
													<img
														src="<?php bloginfo("template_directory") ?>/assets/images/gigglemagazine1.png"
														alt="">
												<?php } ?>
											</a>
										</div>
									</div>
								<?php }
								wp_reset_postdata(); ?>

							</div>
						</div>

					<?php } ?>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .row -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
