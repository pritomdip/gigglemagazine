<?php
/**
 * Template Name: Kids Eat Free
 * @package themeplate
 */
get_header();
?>
<div class="wrapper" id="page-wrapper">
	<div id="content" class="container">
		<div id="primary" class="col-md-8 content-area">
			<main id="main" class="site-main" role="main">
				<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('loop-templates/content', 'page'); ?>
				<?php endwhile; ?>
			</main>
		</div>

		<div id="secondary" class="col-md-4 sidebar widget-area" role="complementary">
			<?php dynamic_sidebar('kids-eat-free-sidebar'); ?>
		</div>

	</div>
</div><!-- Wrapper end -->
<?php get_footer(); ?>
