<?php
/**
 * Template Name: Contact Page
 */
get_header(); ?>

<div class="wrapper contact-page" id="page-wrapper">

	<div class="google-map-area">
		<iframe
			src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4903.503247485155!2d-82.49525795622314!3d29.65305619831864!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88e8beb9680ff93b%3A0xf23cc0fad178e020!2sGiggle+Magazine!5e0!3m2!1sen!2sbd!4v1546504186835"
			width="100%" height="415" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>

	<div id="content" class="container m-tb-40">
		<div class="row">
			<div id="primary" class="col-md-9 content-area">
				<main id="main" class="site-main contact-page-content" role="main">

					<h4>Editorial</h4>
					<p>Do you have an editorial piece you would like to share with our Giggle Magazine readers?  If so,
						please send submission to:  natalie@irvingpublications.com</p>

					<p>Due to the high quantity of emails, we are not able to respond to each submission, however, you
						WILL
						be notified if your story has been chosen to run.</p>

					<h4>Advertising</h4>
					<p>If you are interested in advertising in Alachua County’s Premier Family and Parenting magazine,
						Giggle Magazine, contact us with any questions you might have at:
						advertise@irvingpublications.com
						or 352-505-5821.</p>

					<h4>Ad Design Specs</h4>
					<p>Questions pertaining to advertising specs and formats can be directed to:
						megan@irvingpublications.com or elle@irvingpublications.com</p>

					<h4>Distribution</h4>
					<p>If you would like to be considered as a distribution location, contact Shane Irving.
						shane@irvingpublications.com</p>

					<p>To find where Giggle Magazine is distributed, click here. lorem iss</p>

					<?php //echo do_shortcode( '[contact-form-7 id="18860" title="Contact With US"]' ); ?>

					<div class="contact-form-area">
						<h2 class="contact-form-heading">We’d Love to Hear From You, Get In Touch With Us!</h2>
						<?php echo do_shortcode('[contact-form-7 id="18669" title="Contact With US"]') ?>
					</div>


				</main><!-- #main -->
			</div><!-- #primary -->
			<div id="secondary" class="col-md-3">
				<?php dynamic_sidebar('contact-page-sidebar'); ?>
			</div>
		</div><!-- .row -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
