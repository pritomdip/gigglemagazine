<?php
/**
 * Template Name: Advertise Page
 */
get_header(); ?>
<div class="wrapper contact-page" id="page-wrapper">
	<div id="content" class="container m-tb-40">
		<div class="row">
			<div id="primary" class="col-md-9 content-area">
				<main id="main" class="site-main contact-page-content" role="main">

					<form id="form_11995" class="appnitro top_label" method="post" data-highlightcolor="#FFF7C0"
						  action="/machform/embed.php">
						<div class="form_description">
							<h2>Advertise with us</h2>
							<p>We would love to work with you to grow your business. Please complete the information
								below and one of our account executives will be in touch with you shortly.
							</p>
						</div>
						<ul>
							<li id="li_2" class="simple_name" style="">

								<p class="description">Name
									<span id="required_2" class="required">*</span>
								</p>

								<div class="row">
									<div class="col-md-6">
										<div id="li_5_span_1">
											<label for="element_5_1">First</label>
											<input id="element_5_1" name="element_5_1" class="element text large"
												   value=""
												   type="text"
											>
										</div>
									</div>
									<div class="col-md-6">
										<div id="li_5_span_1">
											<label for="element_5_1">Last</label>
											<input id="element_5_1" name="address" class="element text large"
												   value=""
												   type="text"
											>
										</div>
									</div>
								</div>

							</li>


							<li id="li_3" class="" style="">
								<label class="description" for="element_3">Email <span id="required_3" class="required">*</span></label>
								<div>
									<input id="element_3" name="element_3" class="element text large" type="text"
										   maxlength="255" value="">
								</div>
							</li>


							<li id="li_7" class="" style="">
								<label class="description" for="element_7">Phone </label>
								<div>
									<input id="element_7" name="element_7" class="element text large" type="text"
										   value="">
								</div>
							</li>


							<li id="li_5" class="address" style="">
								<p class="description">Address </p>
								<div>
									<div id="li_5_span_1">
										<label for="element_5_1">Street Address</label>
										<input id="element_5_1" name="element_5_1" class="element text large" value=""
											   type="text"
										>
									</div>
									<div id="li_5_span_1">
										<label for="element_5_1">Address Line 2</label>
										<input id="element_5_1" name="address" class="element text large" value=""
											   type="text"
										>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div id="li_5_span_1">
												<label for="element_5_1">City</label>
												<input id="element_5_1" name="element_5_1" class="element text large"
													   value=""
													   type="text"
												>
											</div>
										</div>
										<div class="col-md-6">
											<div id="li_5_span_1">
												<label for="element_5_1">State / Province / Region</label>
												<input id="element_5_1" name="address" class="element text large"
													   value=""
													   type="text"
												>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div id="li_5_span_1">
												<label for="element_5_1">Postal / Zip Code</label>
												<input id="element_5_1" name="element_5_1" class="element text large"
													   value=""
													   type="text"
												>
											</div>
										</div>
										<div class="col-md-6">
											<div id="li_5_span_1">
												<label for="element_5_6">Country</label>
												<select class="element select large" id="element_5_6"
														name="element_5_6">
													<option value="" selected="selected"></option>
													<option value="United States">United States</option>
													<option value="United Kingdom">United Kingdom</option>
													<option value="Canada">Canada</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</li>

							<li id="li_7" class="" style="">
								<label class="description" for="element_7">Business Name </label>
								<div>
									<input id="element_7" name="element_7" class="element text large" type="text"
										   value="">
								</div>
							</li>

							<li id="li_8" class="multiple_choice" style="">

								<span class="description">Please select which publication you are interested in </span>

								<div>
									<fieldset>

										<legend style="display: none">Please select which publication you are interested
											in
										</legend>

										<div>
											<input id="element_8_1" name="element_8" class="element radio" type="radio"
												   value="1">
											<label class="choice" for="element_8_1">Giggle, Gainesville, FL</label>
										</div>

										<div>
											<input id="element_8_2" name="element_8" class="element radio"
												   type="radio" value="2">
											<label class="choice" for="element_8_2">Giggle, Tallahassee, FL</label>
										</div>

										<div>
											<input id="element_8_3" name="element_8" class="element radio"
												   type="radio" value="3">
											<label class="choice" for="element_8_3">Wellness360, Gainesville, FL</label>
										</div>

									</fieldset>
								</div>
							</li>


							<li id="li_9" class="highlighted" style="">

								<label class="description" for="element_9">Please share some information about your
									company so we can better serve you. </label>
								<div>
                                   <textarea id="element_9" name="element_9" class="element textarea medium" rows="8"
											 cols="90"></textarea>
								</div>
							</li>

							<li id="li_buttons" class="buttons">

								<input id="submit_form" class="button_text" type="submit" name="submit_form"
									   value="Submit">
							</li>

						</ul>
					</form>


				</main>
				<!-- #main -->
			</div>
			<!-- #primary -->
			<div id="secondary" class="col-md-3">
				<?php dynamic_sidebar(''); ?>
			</div>
		</div>
		<!-- .row -->
	</div>
	<!-- Container end -->
</div>
<!-- Wrapper end -->
<?php get_footer(); ?>
