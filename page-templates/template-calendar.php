<?php
/**
 * Template Name: Community Calendar Page
 * @package themeplate
 */
get_header();
?>
<div class="wrapper" id="page-wrapper">
	<div id="content" class="container">

		<div id="primary" class="col-md-9 content-area">
			<main id="main" class="site-main" role="main">
				<div class="calendar-content-wrap p-r-45">
					<?php if (class_exists('EventCalendarPro')) {
						require get_template_directory() . '/template-parts/calendar/widget-events-filter.php';
						?>
						<div id="community-calendar" class="gm-community-calendar-area"></div>
						<?php
						require get_template_directory() . '/template-parts/calendar/calendar-event-list.php';
					} ?>
				</div>
			</main>
		</div>
		<div id="secondary" class="col-md-3 widget-area" role="complementary">
			<?php if (class_exists('EventCalendarPro')) { ?>
				<?php require get_template_directory() . '/template-parts/calendar/calendar-sidebar-button.php';
			} ?>
		</div>

	</div>
</div><!-- Wrapper end -->
<?php get_footer(); ?>
