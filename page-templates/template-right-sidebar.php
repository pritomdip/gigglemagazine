<?php
/**
 * Template Name: Right Sidebar Page
 *
 * Template for displaying a page with right sidebar even if a sidebar widget is published
 *
 * @package themeplate
 */
get_header();
?>
<div class="wrapper" id="page-wrapper">
	<div id="content" class="container">
		<div id="primary" class="col-md-9 content-area">
			<main id="main" class="site-main p-r-45" role="main">
				<?php while (have_posts()) {
					the_post();
					the_content();
				} ?>

			</main><!-- #main -->
		</div><!-- #primary -->

		<div id="secondary" class="col-md-3 widget-area" role="complementary">
			<?php dynamic_sidebar('home-sidebar-1'); ?>
		</div><!-- #secondary -->

	</div><!-- Container end -->
</div><!-- Wrapper end -->
<?php get_footer(); ?>
