<?php
/**
 * Template Name: Printable Page
 */
get_header();
?>
<div class="wrapper" id="page-wrapper">
	<div id="content" class="container">
		<div class="row">
			<div id="primary" class="col-md-12 content-area">
				<main id="main" class="site-main" role="main">
					<?php
					global $post;
					$printable_posts = get_posts(array(
						'posts_per_page' => 20,
						'post_type'      => 'printable',
						'post_status'    => 'publish',
						'orderby'        => 'publish_date',
						'order'          => 'DESC'
					));
					if (!empty($printable_posts)) { ?>
						<div class="featured-work-isotop printable-sec">
							<div class="printable-content">
								<div class="isotope-filters">
									<?php
									$printable_categories = get_terms([
										'taxonomy'   => 'printable_category',
										'hide_empty' => false,
									]);
									if (!empty($printable_categories) && !is_wp_error($printable_categories)) {
										$categories = wp_list_pluck($printable_categories, 'name', 'term_id'); ?>
										<button data-filter="" class="active">All</button>
										<?php
										foreach ($categories as $key => $category) { ?>
											<button
												data-filter=".<?php echo $category; ?>"><?php echo $category ?></button>
										<?php }
									}
									?>
								</div>
								<div class="row">
									<div class="isotope">
										<?php foreach ($printable_posts
													   as $post) {
											setup_postdata($post);
											$post_categories = get_the_terms($post->ID, 'printable_category');
											if (!empty($post_categories)) {
												$post_categories = wp_list_pluck($post_categories, 'name');
												$post_category   = implode(' ', $post_categories);
											} else {
												$post_category = ' ';
											}
											?>
											<div class=" col-md-3 grid-item <?php echo $post_category ?>">
												<a href="<?php echo get_the_permalink(); ?>"
												   class="printable-item">
													<img src="<?php echo get_the_post_thumbnail_url($post) ?>"
														 alt="image">
													<?php //echo get_the_post_thumbnail($post, 'printable-thumb') ?>
													<h4 class="item-title"><?php echo get_the_title($post); ?></h4>
												</a>
											</div>
										<?php }
										wp_reset_postdata(); ?>
									</div>
								</div><!--.row-->

							</div>
						</div>
					<?php } ?>


				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .row -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->


<?php get_footer(); ?>



