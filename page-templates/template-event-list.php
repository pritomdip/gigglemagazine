<?php
/**
 * Template Name:Event Listing Page
 * @package themeplate
 */
$excluded_posts = [];
get_header();
?>
<div class="wrapper" id="page-wrapper">
	<div id="content" class="container">
		<div class="row">
			<div id="primary" class="col-md-9 content-area">
				<main id="main" class="site-main p-r-45" role="main">
					<!--event posts-->
					<?php
					$calendar_posts = get_posts([
						'post_type'      => 'pro_event',
						'posts_per_page' => 6,
						'post_status'    => 'publish',
						'exclude'        => $excluded_posts,
					]);
					if (!empty($calendar_posts)) {
						$calendar_posts_ids = wp_list_pluck($calendar_posts, 'ID');
						$excluded_posts     = array_merge($excluded_posts, $calendar_posts_ids);
						?>
						<div class="content-item-list calendar-area module">
							<div class="row">
								<?php
								foreach ($calendar_posts as $post) {
									setup_postdata($post);
									get_template_part('loop-templates/event-list-post');
								}
								wp_reset_postdata();
								?>
							</div>
						</div>
						<?php
					}
					?>
					<!--event posts end-->
				</main><!-- #main -->
			</div><!-- #primary -->


			<div id="secondary" class="col-md-3 widget-area" role="complementary">
				<div class="sidebar calendar-sidebar">
					<?php dynamic_sidebar('community-calendar-sidebar'); ?>
				</div>
			</div><!-- #secondary -->


		</div><!-- .row -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
