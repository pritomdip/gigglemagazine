<div class="social-media-share-area">
	<div class="row">
		<div class="col-md-5 col-sm-6">
			<div class="social-media-heading">
				<p>SHARE THIS STORY </p>
				<span>choose your platform</span>
			</div>
		</div>
		<div class="col-md-7 col-sm-6">
			<div class="social-media-button">
				<a href="<?php echo gm_get_share_link($post->ID, 'facebook'); ?>" target="_blank"><i
						class="fa fa-facebook"></i></a>
				<a href="<?php echo gm_get_share_link($post->ID, 'twitter'); ?>" target="_blank"><i
						class="fa fa-twitter"></i></a>
				<a href="<?php echo gm_get_share_link($post->ID, 'linkedin'); ?>" target="_blank"><i
						class="fa fa-linkedin"></i></a>
				<a href="<?php echo gm_get_share_link($post->ID, 'reddit'); ?>" target="_blank"><i
						class="fa fa-reddit-alien"></i></a>
				<a href="<?php echo gm_get_share_link($post->ID, 'googleplus'); ?>" target="_blank"><i
						class="fa fa-google-plus"></i></a>
				<a href="<?php echo gm_get_share_link($post->ID, 'tumblr'); ?>" target="_blank"><i
						class="fa fa-tumblr"></i></a>
				<a href="<?php echo gm_get_share_link($post->ID, 'pinterest'); ?>"
				   target="_blank"><i
						class="fa fa-pinterest-p"></i></a>
				<a href="<?php echo gm_get_share_link($post->ID, 'vk'); ?>" target="_blank"><i
						class="fa fa-vk"></i></a>
				<a href="<?php echo gm_get_share_link($post->ID, 'email'); ?>" target="_blank"><i
						class="fa fa-envelope-o"></i></a>
			</div>
		</div>
	</div>
</div>
