<?php
include WPECP_INCLUDES . '/class-ics.php';
$keyword    = !empty($_GET['search_keyword']) ? $_GET['search_keyword'] : '';
$category   = !empty($_GET['event_category']) ? $_GET['event_category'] : '';
$start_date = !empty($_GET['start_date']) ? $_GET['start_date'] : '';
$end_date   = !empty($_GET['end_date']) ? $_GET['end_date'] : '';

$event_query = ecp_get_event_list(
	[
		'event_categories' => !empty($category) ? $category : null,
		'search_keywords'  => !empty($keyword) ? $keyword : null,
		'event_start_date' => $start_date,
		'event_end_date'   => $end_date,
	]
);

if ($event_query->have_posts()) {
	$ics = [];
	foreach ($event_query->get_posts() as $post) {
		setup_postdata($post);
		$event_start_date      = ecp_get_event_meta($post->ID, 'event_start_date');
		$event_end_date        = ecp_get_event_meta($post->ID, 'event_end_date');
		$event_start_time      = ecp_get_event_meta($post->ID, 'event_start_time');
		$event_end_time        = ecp_get_event_meta($post->ID, 'event_end_time');
		$event_location        = ecp_get_event_meta($post->ID, 'event_location');
		$event_sponsor_website = ecp_get_event_meta($post->ID, 'event_sponsor_website');

		$ics[] = new \WebPublisherPro\EventCalendarPro\ECP_ICS(array(
			'location'    => $event_location,
			'description' => esc_html(get_the_excerpt($post->ID)),
			'dtstart'     => $event_start_date,
			'dtend'       => $event_end_date,
			'summary'     => sanitize_text_field(get_the_title($post->ID)),
			'url'         => $event_sponsor_website,
		));

	}

	$ics_string = '';
	foreach ($ics as $ic) {
		$ics_string .= $ic->to_string() . "\n";
	}
}
?>
<div class="gm-event-calendar-filter">
	<div class="ecp-widget-event-filter">
		<form action="<?php echo ecp_get_page_url('events_page'); ?>" method="get">
			<div class="form-group">

				<label for="event_category"><?php _e('Category:', 'event-calendar-pro'); ?></label>
				<select name="event_category" id="event_category" class="form-control">
					<option value="">All</option>
					<?php foreach (ecp_get_all_event_categories() as $slug => $name) { ?>
						<option value="<?php echo $slug; ?>"><?php echo $name; ?></option>
					<?php } ?>
				</select>
			</div>

			<div class="form-group">
				<label for="start_date"><?php _e('Start Date:', 'event-calendar-pro'); ?></label>
				<input type='text' class="form-control" name="start_date" placeholder="2018-11-22"
					   value="<?php echo $start_date; ?>"/>
			</div>
			<div class="form-group">
				<label for="end_date"><?php _e('End Date:', 'event-calendar-pro'); ?></label>
				<input type='text' class="form-control" name="end_date" placeholder="2018-11-22"
					   value="<?php echo $end_date; ?>"/>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="search-field-warp">
						<input type="search" class="form-control" name="search_keyword" placeholder="search"
							   value="<?php echo (!empty($_GET['search_keyword'])) ? $_GET['search_keyword'] : ''; ?>">
						<button class="btn btn-default" type="submit">
							<img src="<?php echo WPECP_ASSETS_URL . '/images/icons/search.svg' ?>" class="ecp-icon"
								 alt="">
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

