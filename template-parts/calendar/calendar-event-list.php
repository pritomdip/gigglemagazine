<div class="gm-community-calendar-event-list">
	<?php
	$date = get_query_var('date');
	$args = array(
		'order_by' => 'event_start_date',
		'order'    => 'DESC',
	);
	if (!empty($date)) {
		$args['exact_date'] = $date;
	} else {
		$args['start_or_end_date'] = date('Y-m-d');
	}
	$posts = ecp_get_event_list($args);
	if ($posts->have_posts()) {

		echo '<div class="ecp-widget-event-list"><ul class="list-unstyled panel-group" id="ecp-widget-event-list-group">';
		?>

		<li><h4><?php echo !empty($date) ? date('D, M j, Y', strtotime($date)) : date('D, M j, Y'); ?></h4></li>
		<?php foreach ($posts->get_posts() as $post) {
			setup_postdata($post);
			include WPECP_TEMPLATES_DIR . '/meta.php';
			?>
			<li class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#ecp-post-<?php echo $post->ID; ?>">
								<span
									class="event-time"><?php echo $starttime . ' - ' . $endtime; ?></span>
							|
							<span
								class="event-title"><?php echo wp_trim_words(get_the_title($post->ID), 15); ?></span>
							<span class="pull-right"><i class="caret"></i></span>
						</a>
					</h4>
				</div>
				<div class="panel-collapse collapse" id="ecp-post-<?php echo $post->ID; ?>">
					<?php echo wp_trim_words(get_the_content($post->ID), 70); ?>

					<?php if (!empty($location)) { ?>
						<p>
						<p>Where</p>
						<?php
						$full_address = $address && $address2 ? $address . ' ' : $address;
						$full_address .= $address2 && $address ? ' ' . $address2 : $address2;
						$full_address .= $city && $address2 ? ' ' . $city : $city;
						$full_address .= $state && $city ? ' ' . $state : $state;
						$full_address .= $zip && $state ? ' ' . $zip : $zip;
						$full_address .= $country && $zip ? ' ' . $country : $country;
						?>
						<?php echo $location ? "<span class='location'>$location</span>" : ''; ?><a
						href="http://maps.google.com/maps?q=<?php echo urlencode($full_address); ?>"
						class="map-btn pull-right"
						target="_blank">View map</a><br>
						<address>
							<?php echo $address ? "<span class='address'>$address, </span>" : '' ?> <?php echo $address2 ? "<span class='address2'>$address2</span>" : '' ?>
							<br>
							<?php echo $city ? "<span class='city'>$city,</span>" : '' ?> <?php echo $zip ? "<span class='zipcode'>$zip,</span>" : '' ?> <?php echo $state ? "<span class='state'>$state</span>" : '' ?><?php echo $country ? ", <span class='country'>$country</span>" : '' ?>
						</address>
						</p>
					<?php } ?>
					<p class="more-link"><a href="<?php echo get_the_permalink($post->ID); ?>">More Information</a></p>
				</div>
			</li>
			<?php
		}
		echo '</ul></div>';
	} else { ?>
		<h4><?php echo !empty($date) ? date('D, M j, Y', strtotime($date)) : date('D, M j, Y'); ?></h4>
		<h4 class="text-center">No Event Found</h4>
	<?php } ?>

</div>
