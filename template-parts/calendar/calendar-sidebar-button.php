
<div class="ecp-widget-event-footer gm-community-calendar-sidebar-button">
	<a href="<?php echo ecp_get_page_url('events_page'); ?>" class="btn btn-block btn-default">Calendar Home <img
			src="<?php echo WPECP_ASSETS_URL . '/images/icons/right-arrow.svg' ?>" class="ecp-icon" alt=""></a>
	<a href="<?php echo ecp_get_page_url('event_submit_page'); ?>" class="btn btn-block btn-default">Submit an event
		<img src="<?php echo WPECP_ASSETS_URL . '/images/icons/right-arrow.svg' ?>" class="ecp-icon" alt=""></a>
	<form action="" method="post">
		<?php wp_nonce_field('calendar_download', 'calendar_download_nonce') ?>
		<input type="hidden" name="action" value="calendar_download">
		<input type="hidden" name="calendar_name" value="Calendar">
		<input type="hidden" name="ics_string" value="<?php echo (isset($ics_string)) ? $ics_string : ''; ?>">
		<button type="submit" class="btn btn-block btn-default">
			Get this Calendar <img src="<?php echo WPECP_ASSETS_URL . '/images/icons/right-arrow.svg' ?>"
								   class="ecp-icon">
		</button>
	</form>
</div>
