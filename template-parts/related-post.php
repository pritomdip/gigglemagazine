<?php
$args    = array(
	'category__in' => wp_get_post_categories($post->ID),
	'numberposts' => 6,
	'post__not_in' => array($post->ID)
);
$related = get_posts($args);
if ($related) { ?>
	<div class="related-stories-area p-tb-30">
		<h3 class="section-title text-center"><span>{</span> related stories <span>}</span></h3>
		<div class="related-stories-inner">
			<div class="row height-auto">
				<?php
				foreach ($related as $post) {
					setup_postdata($post);
					get_template_part('loop-templates/category-post');
				}
				wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
	<?php
}
?>

