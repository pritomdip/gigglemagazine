<div class="sign-up-form-area m-b-30">
	<div class="form-heading-area">
		<div class="angle-arrow-icon">
			<img class="image-responsive" src="<?php bloginfo('template_directory') ?>/assets/images/angle-arrow.png"
				 alt="angle-arrow">
		</div>
		<h3 class="form-heading text-center">WHAT YOU WANT TO KNOW <span>—</span> STRAIGHT TO YOUR INBOX</h3>
	</div>

	<form action="" class="sign-up-form">
		<div class="input-field-group">
			<input type="text" name="last_name" placeholder="FIRST NAME" required>
			<input type="text" name="first_name" placeholder="LAST NAME">
			<input type="text" name="email" placeholder="EMAIL" required>
			<input type="text" name="date_of_birth"
				   placeholder="DATE OF BIRTH/00/00/0000" required>
			<input type="text" name="zip_code" placeholder="ZIP CODE">
		</div><!--input-field-group-->
		<p class="choose-list-title">choose your list</p>

		<div class="checkbox-area">
			<div class="form-group">
				<input type="checkbox" name="sponsors" id="sponsors">
				<label for="sponsors">
					<p class="checkbox-text">
						<strong>From our Sponsors:</strong>
						Get all the latest deals and information from our community
						of
						sponsors and partners.
					</p>
				</label>
			</div>

			<div class="form-group">
				<input type="checkbox" id="parenting">
				<label for="parenting">
					<p class="checkbox-text">
						<strong>Parenting Rocks:</strong>
						Stay in the know about parenting in our community and world!
					</p>
				</label>
			</div>

			<div class="form-group">
				<input type="checkbox" id="planner">
				<label for="planner">
					<p class="checkbox-text">
						<strong>Weekend Planner:</strong>
						Up to date info on all things happening in Ginelle for our
						families!
					</p>
				</label>
			</div>

		</div><!--checkbox-area-->

		<p class="form-info">By submitting this form, you are consenting to receive
			marketing emails
			from: Giggle Magazine, 101 SW 140th Terrace, Unit C, Newberry, FL, 32669
			United States. You can revoke your consent to receive emails at any time
			by using the SafeUnsubscribe® link, found at the bottom of every email.
			Emails are serviced by Constant Contact. Our Privacy Policy.
		</p>

		<div class="sign-up-btn-area">
			<button type="submit" class="btn sign-up-btn">SIGN UP</button>
		</div>

	</form>
</div><!--sign-up-form-area-->
