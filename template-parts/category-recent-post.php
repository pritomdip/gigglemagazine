<?php
global $excluded_posts;
$cat_args   = array(
	"hide_empty" => true,
	'exclude'    => '1',
	'number'     => 5,
);
$categories = get_categories($cat_args);
if (!empty($categories)) { ?>
	<section class="section-padding">
		<div class="categories-area p-tb-30 cs-bg">
			<div class="container">
				<h2 class="category-section-name"><span>[</span> Head for this area: information you need <span>]</span>
				</h2>
				<div class="categories-inner">
					<?php
					foreach ($categories as $category) {
						$post_args             = array(
							'posts_per_page' => 1,
							'cat'            => $category->cat_ID,
							'exclude'        => $excluded_posts
						);
						$category_recent_posts = get_posts($post_args);
						if (!empty($category_recent_posts)) {
							$category_recent_posts_ids = wp_list_pluck($category_recent_posts, 'ID');
							$excluded_posts            = array_merge($excluded_posts, $category_recent_posts_ids);
							foreach ($category_recent_posts as $post) {
								?>
								<div class="category-item">
									<div class="category-item-top">
										<a href="<?php echo get_the_permalink() ?>">
											<?php echo get_the_post_thumbnail($post->ID, 'thumbnail', array('class' => 'img-circle')) ?>
										</a>
									</div>
									<div class="category-item-bottom">
										<div class="meta-category">
											<?php
											$terms = get_the_terms($post->ID, 'category');
											if (!empty($terms)) {
												$term_link = get_term_link($terms[0]->term_id);
												echo '<a class="category-name" href="' . $term_link . '">' . $terms[0]->name . '</a>';
											} ?>
										</div>
										<div class="description">
											<a href="<?php echo get_the_permalink() ?>"><?php echo wp_trim_words(get_the_title(),6); ?></a>
										</div>
									</div>
								</div>
								<?php
							}
						}
					}
					?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>
