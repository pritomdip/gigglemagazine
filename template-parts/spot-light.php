<?php
global $excluded_posts;
$spotlight_args  = array(
	'posts_per_page' => 1,
	'post_type' => 'post',
	'meta_query' => array(
		array(
			'key' => '_spotlight',
			'value' => 'on',
			'compare' => '=',
		)
	),
	'exclude' => $excluded_posts,
	'post_status' => 'publish',
	'orderby' => 'publish_date',
	'order' => 'DESC',
);
$spotlight_posts = get_posts($spotlight_args);
if (!empty($spotlight_posts)) {
	?>
	<div class="spotlight-area">
		<h2 class="section-title">spotlight on<span>&nbsp;&nbsp;<i
					class="fa fa-asterisk"></i></span></h2>
		<?php
		$spotlight_post_ids = wp_list_pluck($spotlight_posts, 'ID');
		$excluded_posts     = array_merge($excluded_posts, $spotlight_post_ids);
		foreach ($spotlight_posts as $post) {
			setup_postdata($post);
			get_template_part('loop-templates/spotlight-post');
		}
		wp_reset_postdata();
		?>
	</div><!--spotlight-area-->
	<?php

} ?>
