<?php
global $excluded_posts;
$recipes_id            = get_cat_ID('recipes');
$recipes_category_link = get_category_link($recipes_id);
$recipe_posts          = get_posts(array(
	'posts_per_page' => 3,
	'post_type' => 'post',
	'category_name' => 'recipes',
	'exclude' => $excluded_posts,
	'post_status' => 'publish',
	'orderby' => 'publish_date',
	'order' => 'DESC'
));
if (!empty($recipe_posts)) {
	?>
	<div class="recipes-post-area border-s-b-1">

		<div class="section-title">
			<h2>Delicious<span> Recipes</span></h2>
		</div>

		<div class="row">
			<?php
			$recipe_posts_ids = wp_list_pluck($recipe_posts, 'ID');
			$excluded_posts   = array_merge($excluded_posts, $recipe_posts_ids);
			foreach ($recipe_posts as $post) {
				setup_postdata($post);
				get_template_part('loop-templates/recipes-post');
			}
			wp_reset_postdata();
			?>
		</div>
		<div class="show-more-area clearfix">
			<p class="pull-right m-b-0">
				<a class="btn show-more-btn"
				   href="<?php echo $recipes_category_link; ?>">see more <span><i
							class="fa fa-angle-right"></i></span></a>
			</p>
		</div>
	</div>
<?php } ?>
