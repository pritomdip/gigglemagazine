<?php
global $excluded_posts;
/*$diy_easy_fun_id = get_cat_ID('DIY easy & fun');
$diy_easy_fun_category_link=get_category_link($diy_easy_fun_id);*/
$diy_easy_fun_posts = get_posts(array(
	'posts_per_page' => 3,
	'post_type' => 'post',
	'category_name' => 'diy',
	'exclude' => $excluded_posts,
	'post_status' => 'publish',
	'orderby' => 'publish_date',
	'order' => 'DESC'
));
if (!empty($diy_easy_fun_posts)) {
	?>
	<div class="diy-easy-fun-area">
		<h2 class="section-title">
			<span>{</span>&nbsp; DIY easy & fun &nbsp;<span>}</span>
		</h2>
		<?php
		$diy_easy_fun_posts_ids = wp_list_pluck($diy_easy_fun_posts, 'ID');
		$excluded_posts         = array_merge($excluded_posts, $diy_easy_fun_posts_ids);
		foreach ($diy_easy_fun_posts as $post) {
			setup_postdata($post);
			get_template_part('loop-templates/diy-easy-fun-post');
		}
		wp_reset_postdata();
		?>
	</div>
<?php } ?>
