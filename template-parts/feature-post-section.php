<?php
global $excluded_posts;
$category_fp_args = array(
	'posts_per_page' => 1,
	'post_type' => 'post',
	'category_name' => 'happy-home',
	'meta_query' => array(
		array(
			'key' => '_feature_post',
			'value' => 'on',
			'compare' => '=',
		)
	),
	'exclude' => $excluded_posts,
	'post_status' => 'publish',
	'orderby' => 'publish_date',
	'order' => 'DESC',
);
$category_f_posts = get_posts($category_fp_args);

if (!empty($category_f_posts)) {
	?>
	<div class="category-feature-post-area">
		<?php
		$category_f_posts_ids = wp_list_pluck($category_f_posts, 'ID');
		$excluded_posts       = array_merge($excluded_posts, $category_f_posts_ids);
		foreach ($category_f_posts as $post) {
			setup_postdata($post);
			get_template_part('loop-templates/category-feature-post');
		}
		wp_reset_postdata();
		?>
	</div>
<?php } ?>
