<?php
$un_categorized_id = get_cat_ID('Uncategorized');
$categories        = get_terms('category', array(
	'orderby' => 'count',
	'order' => 'DESC',
	'hide_empty' => false,
	'number' => 5,
	'exclude' => $un_categorized_id,
));
if (!empty($categories)) {
	?>
	<section class="section-padding">
		<div class="categories-area p-tb-30 cs-bg">
			<div class="container">
				<h2 class="category-section-name"><span>[</span> Head for this area: information you need <span>]</span>
				</h2>
				<div class="categories-inner">
					<?php
					foreach ($categories as $category) {
						?>
						<div class="category-item">
							<div class="category-item-top">
								<?php
								$category_image_link = get_term_meta($category->term_id, '_category_image', true);
								if (!empty($category_image_link)) { ?>
									<a href="<?php echo esc_url(get_term_link($category)); ?>">
										<img class="img-circle" src="<?php echo $category_image_link; ?>" alt="category image">
									</a>
								<?php } ?>
							</div>
							<div class="category-item-bottom">
								<div class="meta-category">
									<a class="category-name"
									   href="<?php echo esc_url(get_term_link($category)); ?>"><?php echo $category->name ?></a>
								</div>
								<p class="description">
									<a href="<?php echo esc_url(get_term_link($category)); ?>"><?php echo $category->description ?></a>
								</p>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div><!--container-->
		</div>
	</section>
	<?php
}
?>
<!--category section end-->
