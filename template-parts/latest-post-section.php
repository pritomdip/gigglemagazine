<?php
global $excluded_posts;
$latest_post = get_posts(array(
	'posts_per_page' => 3,
	'post_type' => 'post',
	'exclude' => $excluded_posts,
	/*'meta_query' => array(
        array(
            'key' => '_category_feature_post',
            'value' => 'on',
            'compare' => '!=',
        )
    ),*/
	'post_status' => 'publish',
	'orderby' => 'publish_date',
	'order' => 'DESC'
));
if (!empty($latest_post)) {
	?>
	<div class="latest-post-area section-padding">

		<div class="section-title">
			<h2><span>The Latest</span> from Giggle Magazine</h2>
		</div>

		<div class="row height-auto">
			<?php
			$latest_post_ids = wp_list_pluck($latest_post, 'ID');
			//$excluded_posts  = array_merge($excluded_posts, $latest_post_ids);
			foreach ($latest_post as $post) {
				setup_postdata($post);
				get_template_part('loop-templates/latest-post');
			}
			wp_reset_postdata();
			?>
		</div>
		<div class="show-more-area clearfix">
			<p class="pull-right m-b-0">
				<a class="btn show-more-btn"
				   href="<?php echo get_site_url() ?>/latest-post">see
					more <span><i
							class="fa fa-angle-right"></i></span></a>
			</p>
		</div>
	</div>
<?php } ?>
