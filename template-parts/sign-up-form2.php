<section>
	<div class="container">
		<!--sign-up-form-area-->
		<div class="sign-up-form-area border-4">
			<h3 class="form-heading form-heading2">WHAT YOU WANT TO KNOW — STRAIGHT TO YOUR INBOX</h3>
			<form action="#" class="sign-up-form sing-up-2">
				<div class="input-field-group">
					<input type="text" name="last_name" placeholder="FIRST NAME" required>
					<input type="text" name="first_name" placeholder="LAST NAME">
					<input type="text" name="date_of_birth"
						   placeholder="DATE OF BIRTH/00/00/0000" style="width: 200px;" required>
					<input type="text" name="zip_code" placeholder="ZIP CODE" style="width: 100px;" required>
					<input type="text" name="email" placeholder="EMAIL" required>
					<button type="submit" class="btn sign-up-btn">SIGN UP</button>
				</div><!--input-field-group-->
			</form>
		</div>
		<!--sign-up-form-area end-->
	</div>
</section>
