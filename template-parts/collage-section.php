<?php
global $excluded_posts;
$collage_id            = get_cat_ID('conception-2-college');
$collage_category_link = get_category_link($collage_id);
$collage_posts         = get_posts(array(
	'posts_per_page' => 3,
	'post_type' => 'post',
	'category_name' => 'conception-2-college',
	'exclude' => $excluded_posts,
	'post_status' => 'publish',
	'orderby' => 'publish_date',
	'order' => 'DESC'
));
if (!empty($collage_posts)) {
	?>
	<div class="collage-post-area border-s-b-1">
		<div class="section-title">
			<h2>Conception <span>to College</span></h2>
		</div>
		<div class="row">
			<?php
			$collage_posts_ids = wp_list_pluck($collage_posts, 'ID');
			$excluded_posts    = array_merge($excluded_posts, $collage_posts_ids);
			foreach ($collage_posts as $post) {
				setup_postdata($post);
				get_template_part('loop-templates/latest-post');
			}
			wp_reset_postdata();
			?>
		</div>
		<div class="show-more-area clearfix">
			<p class="pull-right m-b-0">
				<a class="btn show-more-btn"
				   href="<?php echo $collage_category_link; ?>">see more <span><i
							class="fa fa-angle-right"></i></span></a>
			</p>
		</div>
	</div>
<?php } ?>

