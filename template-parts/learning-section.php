<?php
global $excluded_posts;
$learning_id            = get_cat_ID('learn');
$learning_category_link = get_category_link($learning_id);
$learning_posts         = get_posts(array(
	'posts_per_page' => 3,
	'category_name' => 'learn',
	'post_type' => 'post',
	'exclude' => $excluded_posts,
	'post_status' => 'publish',
	'orderby' => 'publish_date',
	'order' => 'DESC'
));
if (!empty($learning_posts)) {
	?>
	<div class="learning-post-area border-s-b-1">
		<div class="section-title">
			<h2><span>Learning</span> Connection</h2>
		</div>

		<div class="row">
			<?php
			$learning_posts_ids = wp_list_pluck($learning_posts, 'ID');
			$excluded_posts     = array_merge($excluded_posts, $learning_posts_ids);
			foreach ($learning_posts as $post) {
				setup_postdata($post);
				get_template_part('loop-templates/latest-post');
			}
			wp_reset_postdata();
			?>
		</div>
		<div class="show-more-area clearfix">
			<p class="pull-right m-b-0">
				<a class="btn show-more-btn"
				   href="<?php echo $learning_category_link; ?>">see more <span><i
							class="fa fa-angle-right"></i></span></a>
			</p>
		</div>
	</div>
<?php } ?>
