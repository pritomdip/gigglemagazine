<?php
/**
 * themeplate functions and definitions
 *
 * @package themeplate
 */
/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/custom-comments.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';

/**
 * Load WooCommerce functions.
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Load meta box class.
 */
require get_template_directory() . '/inc/class-metabox.php';
if (class_exists('KCP_metaBox')) {
	new KCP_metaBox();
}
/**
 * Load short code class.
 */
require get_template_directory() . '/inc/class-shortcode.php';
if (class_exists('KCP_shortCode')) {
	new KCP_shortCode();
}
/**
 * Load short code class.
 */
require get_template_directory() . '/inc/class-register-directory.php';
if (class_exists('KCP_directoryListing')) {
	new KCP_directoryListing();
}

/**
 * Load webDirectoryPro functions.
 */
if (class_exists('WebDirectoryPro')) {
	require get_template_directory() . '/directory/directory.php';
}

//plugins activator
require get_template_directory() . '/inc/plugins-activator.php';




