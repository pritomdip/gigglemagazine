<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package themeplate
 */

$excluded_posts = [];
get_header();
?>

<div class="wrapper" id="archive-wrapper">

	<div id="content" class="category-archive">

		<div id="primary" class="content-area">

			<main id="main" class="site-main" role="main">


				<div class="container">
					<div class="feature-slider-inner owl-carousel">
						<?php
						if (is_category()) {
							$taxonomy_name = 'category';
						} elseif (is_tag()) {
							$taxonomy_name = 'post_tag';
						}

						$args          = array(
							'posts_per_page' => 5,
							'post_type'      => 'post',
							'post_status'    => 'publish',
							'meta_query'     => array(
								array(
									'key'     => '_feature_post',
									'value'   => 'on',
									'compare' => '=',
								)
							),
							'exclude'        => $excluded_posts,
							'tax_query'      => [
								[
									'taxonomy' => $taxonomy_name,
									'field'    => 'name',
									'terms'    => get_queried_object()->name,
								]
							]
						);
						$feature_posts = get_posts($args);
						if (!empty($feature_posts)) {
							$featured_post_ids = wp_list_pluck($feature_posts, 'ID');
							$excluded_posts    = array_merge($excluded_posts, $featured_post_ids);
							foreach ($feature_posts as $post) {
								setup_postdata($post);
								get_template_part('loop-templates/slider-feature-post');
							}
							wp_reset_postdata();
						} ?>

					</div><!--feature-slider-inner-->
				</div><!--container-->


				<div class="container">
					<div class="row">
						<div class="col-md-9">

							<!--hero-banner-->
							<div class="hero-banner">
								<a href="#">
									<img
										src="<?php bloginfo('template_directory') ?>/assets/images/hero-banner-800X200.png"
										alt="banner-image">
								</a>
							</div>
							<!--hero-banner end-->

							<?php
							$first_sec_posts = get_posts(
								array(
									'posts_per_page' => 3,
									'post_type'      => 'post',
									'post_status'    => 'publish',
									'exclude'        => $excluded_posts,
									/*	'meta_query' => array(
                                            'relation' => 'AND',
                                            array(
                                                'key' => '_slider_feature_post',
                                                'value' => 'on',
                                                'compare' => '!=',
                                            ),
                                            array(
                                                'key' => '_category_feature_post',
                                                'value' => 'on',
                                                'compare' => '!=',
                                            )
                                        ),*/
									'tax_query'      => [
										[
											'taxonomy' => $taxonomy_name,
											'field'    => 'name',
											'terms'    => get_queried_object()->name,
										]
									]
								)
							);
							if ($first_sec_posts) {
								?>
								<div class="first-section-area section-padding3">
									<div class="row height-auto">
										<?php
										$first_sec_post_ids = wp_list_pluck($first_sec_posts, 'ID');
										$excluded_posts     = array_merge($excluded_posts, $first_sec_post_ids);
										foreach ($first_sec_posts as $post) {
											setup_postdata($post);
											get_template_part('loop-templates/category-post');
										}
										wp_reset_postdata();
										?>
									</div><!--row-->
								</div><!--first-section-area-->
							<?php } ?>

							<!--category feature post-->
							<?php
							$feature_post_args = array(
								'posts_per_page' => 1,
								'post_type'      => 'post',
								'post_status'    => 'publish',
								'meta_query'     => array(
									array(
										'key'     => '_feature_post',
										'value'   => 'on',
										'compare' => '=',
									)
								),
								'exclude'        => $excluded_posts,
								'tax_query'      => [
									[
										'taxonomy' => $taxonomy_name,
										'field'    => 'name',
										'terms'    => get_queried_object()->name,
									]
								]
							);
							$feature_posts     = get_posts($feature_post_args);
							if (!empty($feature_posts)) {
								$featured_post_ids = wp_list_pluck($feature_posts, 'ID');
								$excluded_posts    = array_merge($excluded_posts, $featured_post_ids);
								foreach ($feature_posts as $post) {
									setup_postdata($post);
									get_template_part('loop-templates/category-feature-post');
								}
								wp_reset_postdata();
							}
							?>
						</div><!--col-md-9-->

						<div class="col-md-3">
							<div class="sidebar category-sidebar1 p-b-30">
								<?php dynamic_sidebar('category-sidebar-1'); ?>
							</div><!--sidebar-->
						</div>

					</div><!--row-->
				</div><!--container-->


				<!--category section-->
				<div class="ca-category-section">
					<?php
					get_template_part('template-parts/category-recent-post');
					/*get_template_part('template-parts/category-section');*/
					?>
				</div>
				<!--category section end-->


				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<!--add-banner-->
							<div class="add-banner">
								<a href="#">
									<img class="image-responsive"
										 src="<?php bloginfo('template_directory') ?>/assets/images/banner2.png"
										 alt="banner-image">
								</a>
							</div>
							<!--add-banner end-->
							<div class="row height-auto">
								<?php
								$second_sec_posts = get_posts(
									array(
										'posts_per_page' => 9,
										'post_type'      => 'post',
										'post_status'    => 'publish',
										'exclude'        => $excluded_posts,
										'tax_query'      => [
											[
												'taxonomy' => $taxonomy_name,
												'field'    => 'name',
												'terms'    => get_queried_object()->name,
											]
										]
									)
								);

								if ($second_sec_posts) {
									$second_sec_post_ids = wp_list_pluck($second_sec_posts, 'ID');
									$excluded_posts      = array_merge($excluded_posts, $second_sec_post_ids);
									foreach ($second_sec_posts as $post) {
										setup_postdata($post);
										get_template_part('loop-templates/category-post');
									}
									wp_reset_postdata();

								}
								?>
							</div>
						</div><!--col-md-9-->

						<div class="col-md-3">
							<div class="sidebar category-sidebar2 p-b-30">
								<!--sign-up-form-->
								<?php get_template_part('template-parts/sign-up-form'); ?>
								<!--sign-up-form end-->
								<?php dynamic_sidebar('home-sidebar-2'); ?>
							</div><!--sidebar-->
						</div>
					</div><!--row-->
				</div><!--container-->


				<!--sign-up-form-->
				<div class="ca-sign-up-form p-b-60">
					<?php get_template_part('template-parts/sign-up-form2');
					?>
				</div>
				<!--sign-up-form end-->


				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="row height-auto">
								<?php
								$third_sec_posts = get_posts(
									array(
										'posts_per_page' => 9,
										'post_type'      => 'post',
										'post_status'    => 'publish',
										'exclude'        => $excluded_posts,
										'tax_query'      => [
											[
												'taxonomy' => $taxonomy_name,
												'field'    => 'name',
												'terms'    => get_queried_object()->name,
											]
										]
									)
								);

								if ($third_sec_posts) {
									$third_sec_post_ids = wp_list_pluck($third_sec_posts, 'ID');
									$excluded_posts     = array_merge($excluded_posts, $third_sec_post_ids);
									foreach ($third_sec_posts as $post) {
										setup_postdata($post);
										get_template_part('loop-templates/category-post');
									}
									wp_reset_postdata();
								}
								?>
							</div>
							<!--add-banner-->
							<div class="add-banner">
								<a href="#">
									<img class="image-responsive"
										 src="<?php bloginfo('template_directory') ?>/assets/images/banner3.png"
										 alt="banner-image">
								</a>
							</div>
							<!--add-banner end-->

						</div><!--col-md-9-->
						<div class="col-md-3">
							<div class="sidebar category-sidebar3 p-b-30">
								<?php dynamic_sidebar('category-sidebar-3'); ?>
							</div><!--sidebar-->
						</div>

					</div><!--row-->
				</div><!--container-->


			</main><!-- #main -->

		</div><!-- #primary -->


	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
