window.Project = (function (window, document, $, undefined) {
	'use strict';

	var app = {
		initialize: function () {
			app.initHamburgerMenu();
			app.initFeatureSlider('.feature-slider-inner');
			app.initMatchHeight('.adjust-post-card-height');
			app.initMatchHeight('.adjust-category-post-height');
			app.initMenuItemActive();
			//app.initDropDownHover();
			$(window).on('resize', app.hoverable_dropdown);
			app.hoverable_dropdown();
			app.initIsotope();
			app.initCalendar();
			app.initMagnific();
			app.menubarPosition();
		},

		menubarPosition: function () {
			$(window).scroll(function () {
				if ($(window).scrollTop() > 200) {
					$('.wrapper-navbar').addClass('sticky-header');
				} else if ($(window).scrollTop() < 50) {
					$('.wrapper-navbar').removeClass('sticky-header');
				}
			});
		},
		initMagnific: function () {
			$('.image-large').magnificPopup({
				type: 'image'
			});

			$('.play-video').magnificPopup({
				type: 'iframe'
			});

			$.extend(true, $.magnificPopup.defaults, {
				iframe: {
					patterns: {
						youtube: {
							index: 'youtube.com/',
							id: 'v=',
							src: 'http://www.youtube.com/embed/%id%?autoplay=1'
						}
					}
				}
			});
		},


		initCalendar: function () {
			$('#community-calendar').fullCalendar();
		},

		initIsotope: function () {
			var $isotopeContainer = $('.isotope');
			$('#container').imagesLoaded(function () {
				$isotopeContainer.isotope({
					resizable: true,
					itemSelector: '.grid-item',
					masonry: {
						columnWidth: '.grid-item'
					}
				});
			});

			// bind filter button click
			$('.isotope-filters').on('click', 'button', function () {
				var filterValue = $(this).attr('data-filter');
				$isotopeContainer.isotope({filter: filterValue});
			});

			// change active class on buttons
			$('.isotope-filters').each(function (i, buttonGroup) {
				var $buttonGroup = $(buttonGroup);
				$buttonGroup.on('click', 'button', function () {
					$buttonGroup.find('.active').removeClass('active');
					$(this).addClass('active');
				});
			});
		},
		hoverable_dropdown: function () {
			if ($(window).width() < 768) {
				$('.dropdown-toggle').attr('data-toggle', 'dropdown');
			} else {
				$('.dropdown-toggle').removeAttr('data-toggle dropdown');
			}
		},

		/*	initDropDownHover: function () {
                $('.main-menu li.dropdown').hover(function () {
                    $(this).addClass('open');
                }, function () {
                    $(this).removeClass('open');
                });
            },*/
		initMenuItemActive: function () {
			$('.main-menu li').on('click', function () {
				$(this).addClass('active').siblings().removeClass('active');
			});
		},
		initHamburgerMenu: function () {
			$('.hamburger-icon').on('click', function () {
				$('.hamburger-menu, .hamburger-full-body-overlay').addClass('active');
				return false;
			});
			$('.hamburger-menu-close, .hamburger-full-body-overlay').on('click', function () {
				$('.hamburger-menu, .hamburger-full-body-overlay').removeClass('active');
			});
		},
		initMatchHeight: function ($selector) {
			$($selector).matchHeight({
				property: 'height'
			});
		},
		initFeatureSlider: function ($selector) {
			$($selector).owlCarousel({
				items: 1,
				loop: true,
				autoplay: true,
				autoplayTimeout: 2000,
				slideTransition: 'ease',
				autoplaySpeed: 1000,
				autoplayHoverPause: true,
				margin: 10,
				nav: false,
				dots: true,
				navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
			});
		}
	};

	$(document).ready(app.initialize);

	return app;
})(window, document, jQuery);


