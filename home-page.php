<?php
/**
 * Template Name: Home page template
 * @package themeplate
 */
$excluded_posts = [];
get_header();
?>

<div class="wrapper" id="page-wrapper">
	<div id="content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">


				<!--slider feature post-->
				<section>
					<div class="container">
						<div class="feature-slider-inner owl-carousel">
							<?php
							$args          = array(
								'posts_per_page' => 5,
								'post_type'      => 'post',
								'meta_query'     => array(
									array(
										'key'     => '_feature_post',
										'value'   => 'on',
										'compare' => '=',
									)
								),
								'exclude'        => $excluded_posts,
								'post_status'    => 'publish',
								'orderby'        => 'publish_date',
								'order'          => 'DESC',
							);
							$feature_posts = get_posts($args);
							if (!empty($feature_posts)) {
								$featured_post_ids = wp_list_pluck($feature_posts, 'ID');
								$excluded_posts    = array_merge($excluded_posts, $featured_post_ids);
								foreach ($feature_posts as $post) {
									setup_postdata($post);
									get_template_part('loop-templates/slider-feature-post');
								}
								wp_reset_postdata();
							} ?>

						</div>
						<!--slider feature post end-->
					</div><!--container-->
				</section>

				<section>
					<div class="container">
						<div class="row">
							<div class="col-md-9">
								<!--hero-banner-->
								<div class="hero-banner">
									<a href="#">
										<img
											src="<?php bloginfo('template_directory') ?>/assets/images/hero-banner-800X200.png"
											alt="banner-image">
									</a>
								</div>
								<!--latest post section-->
								<?php get_template_part('template-parts/latest-post-section'); ?>
								<!--feature post section-->
								<?php get_template_part('template-parts/feature-post-section'); ?>
							</div><!--col-md-9-->

							<div class="col-md-3">
								<div class="sidebar home-sidebar1 p-b-30">
									<?php dynamic_sidebar('home-sidebar-1'); ?>
								</div><!--sidebar-->

							</div><!--col-md-3-->
						</div><!--row-->
					</div><!--container-->
				</section>


				<!--category section-->
				<?php
				get_template_part('template-parts/category-recent-post');
				/*get_template_part('template-parts/category-section.php');*/
				?>
				<!--category section end-->

				<section>
					<div class="container">
						<div class="row">
							<div class="col-md-9">
								<!--recipe post-->
								<?php
								get_template_part('template-parts/recipe-section');
								?>
								<!--add-banner-->
								<div class="add-banner">
									<a href="#">
										<img class="image-responsive"
											 src="<?php bloginfo('template_directory') ?>/assets/images/banner2.png"
											 alt="banner-image">
									</a>
								</div>
								<!--collage section-->
								<?php get_template_part('template-parts/collage-section'); ?>
								<!--add-banner-->
								<div class="add-banner">
									<a href="#">
										<img class="image-responsive"
											 src="<?php bloginfo('template_directory') ?>/assets/images/banner3.png"
											 alt="banner-image">
									</a>
								</div>
								<!--add-banner end-->
							</div><!--col-md-9-->

							<div class="col-md-3">
								<div class="sidebar home-sidebar2 p-b-30">
									<!--sign-up-form-->
									<?php get_template_part('template-parts/sign-up-form'); ?>
									<!--sign-up-form end-->

									<?php dynamic_sidebar('home-sidebar-2'); ?>

								</div><!--sidebar-->

							</div><!--col-md-3-->
						</div><!--row-->
					</div><!--container-->
				</section>

				<!--sign-up-form-->
				<?php get_template_part('template-parts/sign-up-form2'); ?>
				<!--sign-up-form end-->

				<section class="section-padding2">
					<div class="container">
						<div class="row height-auto">
							<div class="col-md-6">
								<!--spot-light-->
								<?php get_template_part('template-parts/spot-light'); ?>
							</div><!--col-md-6-->

							<div class="col-md-6">
								<!--diy-easy-fun section-->
								<?php get_template_part('template-parts/diy-easy-fun-section'); ?>
							</div><!--col-md-6-->

						</div><!--.row-->
					</div><!--.container-->
				</section>


				<!--learning section-->
				<section>
					<div class="container">
						<div class="row">
							<div class="col-md-9">
								<?php get_template_part('template-parts/learning-section'); ?>
							</div><!--col-md-9-->
							<div class="col-md-3">
								<div class="sidebar home-sidebar3 p-b-30 m-t-18">
									<div class="widget">
										<h3 class="widget-title">Our Directories</h3>
										<div class="directory-listing-menu">
											<ul>
												<li><a href="<?php echo get_site_url(); ?>/education-childcare">Education
														& Childcare</a></li>
												<li><a href="<?php echo get_site_url(); ?>/fun-around-town">Fun Around
														Town</a></li>
												<li><a href="<?php echo get_site_url(); ?>/parties-events">Parties &
														Events</a></li>
												<li><a href="<?php echo get_site_url(); ?>/pregnancy-baby">Pregnancy &
														baby</a></li>
											</ul>
										</div>
									</div>
								</div><!--sidebar-->
							</div>
						</div>
					</div>
				</section><!--learning section end-->

				<!--add-banner-->
				<div class="container">
					<div class="add-banner2">
						<a href="#">
							<img class="image-responsive"
								 src="<?php bloginfo('template_directory') ?>/assets/images/banner4.png"
								 alt="banner-image">
						</a>
					</div>
				</div>
				<!--add-banner end-->

			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>




