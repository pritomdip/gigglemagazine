<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package themeplate
 */

get_header();
global $wp_query;
$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>

<div class="wrapper" id="archive-wrapper">
	<div id="content" class="container">
		<div class="row">
			<div id="primary"
				 class="<?php if (is_active_sidebar('sidebar-1')) : ?>col-md-9<?php else : ?>col-md-12<?php endif; ?> content-area">
				<main id="main" class="site-main p-r-45" role="main">

					<?php if (have_posts()) { ?>
						<div class="row height-auto">
							<?php while (have_posts()) : the_post(); ?>
								<?php
								get_template_part('loop-templates/content', 'blog');
								?>
							<?php endwhile; ?>
							<div class="custom-pagination text-center">
								<div class="pagination">
									<?php
									$big = 999999999;
									echo paginate_links(array(
										'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
										'format'    => '?paged=%#%',
										'current'   => max(1, $current_page),
										'total'     => $wp_query->max_num_pages,
										'prev_text' => '<span><i class="fa fa-angle-double-left"></i></span>',
										'next_text' => '<span><i class="fa fa-angle-double-right"></i></span>'
									));
									?>
								</div>
							</div>
						</div>
					<?php } else { ?>
						<?php get_template_part('loop-templates/content', 'none'); ?>
					<?php } ?>

				</main><!-- #main -->
			</div><!-- #primary -->

			<?php get_sidebar(); ?>

		</div> <!-- .row -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->
<?php get_footer(); ?>
