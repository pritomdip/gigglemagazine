<?php
/**
 * The template for displaying all single posts.
 *
 * @package themeplate
 */

get_header(); ?>
<div class="wrapper" id="single-wrapper">
	<div id="content" class="container">
		<div class="row">
			<div id="primary" class="col-md-9 content-area">

				<main id="main" class="site-main" role="main">
					<?php while (have_posts()) : the_post(); ?>
						<?php get_template_part('loop-templates/content', 'single'); ?>
					<?php endwhile; ?>
				</main><!-- #main -->

			</div><!-- #primary -->

			<div id="secondary" class="col-md-3">
				<div class="sidebar calendar-sidebar">
					<?php dynamic_sidebar('community-calendar-sidebar'); ?>
				</div><!--single-sidebar-->
			</div>

		</div><!-- .row -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
