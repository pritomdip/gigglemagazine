<?php
/**
 * The template for displaying all single posts.
 *
 * @package themeplate
 */
get_header();
?>
<div class="single-directory-wrapper" id="page-wrapper">
	<?php while (have_posts()) : the_post(); ?>

		<?php the_content(); ?>

	<?php endwhile; ?>
</div>

<?php get_footer(); ?>
