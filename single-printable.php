<?php
/**
 * The template for displaying all single posts.
 *
 * @package themeplate
 */

get_header(); ?>
<div class="wrapper" id="single-wrapper">

	<div id="content" class="container">

		<div class="row">

			<div id="primary"
				 class="<?php if (is_active_sidebar('printable-page-sidebar-1')) : ?>col-md-9<?php else : ?>col-md-12<?php endif; ?> content-area">
				<main id="main" class="site-main" role="main">
					<?php while (have_posts()) : the_post(); ?>
						<?php get_template_part('loop-templates/printable', 'single'); ?>
					<?php endwhile; ?>


					<!--related stories-->
					<?php //get_template_part('template-parts/related-post'); ?>

				</main><!-- #main -->
			</div><!-- #primary -->


			<div id="secondary" class="col-md-3 widget-area" role="complementary">
				<?php dynamic_sidebar('printable-page-sidebar-1'); ?>
			</div>


		</div><!-- .row -->
	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
