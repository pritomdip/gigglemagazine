<?php

class KCP_metaBox
{
	/**
	 * The single instance of the class.
	 *
	 * @var chartdynamix
	 * @since 1.0.0
	 */
	protected static $instance = null;

	/**
	 * MetaBox constructor.
	 */
	public function __construct()
	{
		//post-meta
		add_action('add_meta_boxes', array($this, 'register_meta_boxes'));
		add_action('save_post', array($this, 'save_post_meta'));

		//term-meta
		add_action('category_add_form_fields', array($this, 'categories_add_meta_fields'), 10, 1);
		add_action('category_edit_form_fields', array($this, 'categories_edit_meta_fields'), 10, 2);
		add_action('create_category', array($this, 'save_category_term_meta'), 10, 2);
		add_action('edited_category', array($this, 'save_category_term_meta'), 10, 2);

	}

	public function register_meta_boxes()
	{
		add_meta_box('post_settings', 'Post Settings', array($this, 'render_item_meta_metabox'), 'post', 'side', 'high');
		add_meta_box('post_settings', 'Post Meta', array($this, 'render_past_issues_meta_metabox'), 'past_issues', 'normal', 'high');
		add_meta_box('post_normal_meta', 'Post Meta', array($this, 'render_normal_post_meta'), 'post', 'normal', 'high');
		add_meta_box('PDF Upload', 'PDF Upload', array($this, 'render_printable_meta_box'), 'printable', 'normal', 'high');
	}

	public function render_normal_post_meta($post)
	{
		$subtitle = get_post_meta($post->ID, 'subtitle', true);
		?>
		<div class="form-group">
			<p>
				<label for="subtitle"><strong>Subtitle</strong></label>
				<br>
				<input type="text" name="subtitle" id="subtitle" class="regular-text"
					   value="<?php echo $subtitle ?>">
			</p>
		</div>
		<?php
	}

	public function render_printable_meta_box($post)
	{
		$pdf_upload = get_post_meta($post->ID, '_pdf_upload', true);
		?>
		<div class="form-field">
			<label for="pdf"><?php _e('PDF Upload', 'themeplate'); ?></label>
			<input type="text" name="_pdf_upload" id="pdf-upload-add" class="field"
				   value="<?php echo $pdf_upload ?>"/>
			<a href="#" id="set-pdf-upload" class="thickbox"
			   style="text-align: left;display: inline-block;margin: 5px 0;">Set PDF Upload</a>
		</div>
		<script>
			jQuery('#set-pdf-upload').click(function () {
				var send_attachment_bkp = wp.media.editor.send.attachment;
				wp.media.editor.send.attachment = function (props, attachment) {
					jQuery('#pdf-upload-add').val(attachment.url);
					wp.media.editor.send.attachment = send_attachment_bkp;
				};
				wp.media.editor.open();
				return false;
			});
		</script>
		<?php

	}

	public function render_item_meta_metabox($post)
	{
		$feature_post = get_post_meta($post->ID, '_feature_post', true);
		$spotlight    = get_post_meta($post->ID, '_spotlight', true);
		$sponsored    = get_post_meta($post->ID, '_sponsored', true);
		?>
		<div class="form-group">
			<p>
				<label class="selectit">
					<input id="feature_post" type="checkbox" class="form-field" name="_feature_post"
						   value='on' <?php if ('on' == $feature_post) echo 'checked="checked"'; ?>>&nbsp;Available
					for
					feature post.
				</label>
			</p>
		</div>

		<div class="form-group">
			<p>
				<label class="selectit">
					<input id="spotlight" type="checkbox" class="form-field" name="_spotlight"
						   value='on' <?php if ('on' == $spotlight) echo 'checked="checked"'; ?>>&nbsp;Available
					for spotlight.
				</label>
			</p>
		</div>

		<div class="form-group">
			<p>
				<label class="selectit">
					<input id="sponsored" type="checkbox" class="form-field" name="_sponsored"
						   value='on' <?php if ('on' == $sponsored) echo 'checked="checked"'; ?>>&nbsp;Available
					for sponsored.
				</label>
			</p>
		</div>

		<?php
	}

	public function render_past_issues_meta_metabox($post)
	{
		$website_url = get_post_meta($post->ID, '_website_url', true);
		?>
		<div class="form-group">
			<p>
				<label for="website_url"><strong>Website URL</strong></label>
				<br>
				<input type="text" name="_website_url" id="website_url" class="regular-text"
					   value="<?php echo $website_url ?>" required="required">
			</p>
		</div>
		<?php
	}

	public function save_post_meta($post_id)
	{
		$feature_post = empty($_POST['_feature_post']) ? '' : esc_attr($_POST['_feature_post']);
		$spotlight    = empty($_POST['_spotlight']) ? '' : esc_attr($_POST['_spotlight']);
		$sponsored    = empty($_POST['_sponsored']) ? '' : esc_attr($_POST['_sponsored']);
		$subtitle     = empty($_POST['subtitle']) ? '' : esc_attr($_POST['subtitle']);

		update_post_meta($post_id, '_feature_post', $feature_post);
		update_post_meta($post_id, '_spotlight', $spotlight);
		update_post_meta($post_id, '_sponsored', $sponsored);
		update_post_meta($post_id, 'subtitle', $subtitle);


		$website_url = empty($_POST['_website_url']) ? '' : esc_attr($_POST['_website_url']);
		update_post_meta($post_id, '_website_url', $website_url);

		$pdf_upload = empty($_POST['_pdf_upload']) ? '' : esc_attr($_POST['_pdf_upload']);
		update_post_meta($post_id, '_pdf_upload', $pdf_upload);


	}

	//term-meta-field
	public function categories_add_meta_fields()
	{
		?>
		<div class="form-field">
			<label for="street"><?php _e('Category Image', 'themeplate'); ?></label>
			<input type="text" name="_category_image" id="category-image-add" class="ever-field"
				   value=""/>
			<a href="#" id="set-category-image-add" class="thickbox"
			   style="text-align: left;display: inline-block;margin: 5px 0;">Set Category Image</a>
		</div>
		<script>
			jQuery('#set-category-image-add').click(function () {
				var send_attachment_bkp = wp.media.editor.send.attachment;
				wp.media.editor.send.attachment = function (props, attachment) {
					jQuery('#category-image-add').val(attachment.url);
					wp.media.editor.send.attachment = send_attachment_bkp;
				};
				wp.media.editor.open();
				return false;
			});
		</script>
		<?php
	}

	public function categories_edit_meta_fields($term)
	{

		$term_id       = $term->term_id;
		$download_file = get_term_meta($term_id, '_category_image', true);
		?>
		<tr class="form-field">
			<th scope="row" valign="top"><label for="street"><?php _e('Category Image', 'themeplate'); ?></label></th>
			<td>
				<input type="text" name="_category_image" id="category-image-edit" class="ever-field"
					   value="<?php echo $download_file; ?>"/>

				<a href="#" id="set-category-image-edit" class="thickbox"
				   style="text-align: left;display: inline-block;margin: 5px 0;">Set Category Image</a>
			</td>
		</tr>
		<script>
			jQuery('#set-category-image-edit').click(function () {
				var send_attachment_bkp = wp.media.editor.send.attachment;
				wp.media.editor.send.attachment = function (props, attachment) {
					jQuery('#category-image-edit').val(attachment.url);
					wp.media.editor.send.attachment = send_attachment_bkp;
				};
				wp.media.editor.open();
				return false;
			});
		</script>

		<?php
	}

	public function save_category_term_meta($term_id)
	{
		$download_file = empty($_POST['_category_image']) ? '' : esc_sql($_POST['_category_image']);
		update_term_meta($term_id, '_category_image', $download_file);

	}

}


