<?php

/**
 * Class ShortCode
 */
class KCP_shortCode
{

	protected static $instance = null;

	/**
	 * ShortCode constructor.
	 */
	public function __construct()
	{
		add_shortcode('gm_sign_up_form', array($this, 'render_gm_sign_up_form'));
		add_shortcode('gm_directory_listing', array($this, 'render_gm_directory_listing'));
	}


	/**
	 * it's sign up form short-code
	 * [gm_sign_up_form]
	 */
	public function render_gm_sign_up_form($atts)
	{
		/*$cat_atts = shortcode_atts(array(
			'title' => '',
			'tag_slug' => '',
			'limit' => 5,
			'layout' => 'grid',
			'type' => 'post',
			'pagination' => 'on'
		), $atts, 'gigglemagazine');
		extract($cat_atts);*/
		ob_start();
		require get_template_directory() . "/template-parts/sign-up-form.php";
		$html = ob_get_contents();
		ob_get_clean();
		return $html;
	}

	/**
	 * it's sign up form short-code
	 * [gm_directory_listing]
	 */
	public function render_gm_directory_listing()
	{
		ob_start();
		require get_template_directory() . "/template-parts/directory-listing.php";
		$html = ob_get_contents();
		ob_get_clean();
		return $html;
	}


}
