<?php
/**
 * Declaring widgets
 *
 *
 * @package themeplate
 */
function themeplate_widgets_init()
{
	register_sidebar(array(
		'name'          => __('Sidebar', 'themeplate'),
		'id'            => 'sidebar-1',
		'description'   => 'Sidebar widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Home Sidebar 1', 'themeplate'),
		'id'            => 'home-sidebar-1',
		'description'   => 'Home Sidebar 1 widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Home Sidebar 2', 'themeplate'),
		'id'            => 'home-sidebar-2',
		'description'   => 'Home Sidebar 2 widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Kids Eat Free Sidebar', 'themeplate'),
		'id'            => 'kids-eat-free-sidebar',
		'description'   => 'Kids Eat Free Sidebar Widget',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Community Calendar Sidebar', 'themeplate'),
		'id'            => 'community-calendar-sidebar',
		'description'   => 'community calendar sidebar widget',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));


	register_sidebar(array(
		'name'          => __('Category Archive Sidebar 1', 'themeplate'),
		'id'            => 'category-sidebar-1',
		'description'   => 'Category Archive Sidebar 1 widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Category Archive Sidebar 3', 'themeplate'),
		'id'            => 'category-sidebar-3',
		'description'   => 'Category Archive Sidebar 3 widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Single Page Sidebar 1', 'themeplate'),
		'id'            => 'single-page-sidebar-1',
		'description'   => 'Single Page Sidebar 1 widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Printable Page Sidebar', 'themeplate'),
		'id'            => 'printable-page-sidebar-1',
		'description'   => 'Printable Page Sidebar widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Contact Page Sidebar', 'themeplate'),
		'id'            => 'contact-page-sidebar',
		'description'   => 'Contact Page Sidebar widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));


	register_sidebar(array(
		'name'          => __('Footer 1', 'themeplate'),
		'id'            => 'footer-1',
		'description'   => 'Widget area in the footer area 1',
		'before_widget' => '<div class="footer-widget-block">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Footer 2', 'themeplate'),
		'id'            => 'footer-2',
		'description'   => 'Widget area in the footer area 2',
		'before_widget' => '<div class="footer-widget-block">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Footer-3', 'themeplate'),
		'id'            => 'footer-3',
		'description'   => 'Widget area in the footer area 3',
		'before_widget' => '<div class="footer-widget-block">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __('Footer Site Info', 'themeplate'),
		'id'            => 'footer-site-info',
		'description'   => 'Widget area footer site info',
		'before_widget' => '<div class="footer-site-info">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-widget-title">',
		'after_title'   => '</h3>',
	));


}

add_action('widgets_init', 'themeplate_widgets_init');
