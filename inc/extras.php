<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package themeplate
 */
/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */

//global variable for post class
//================================
$feature_post_class          = array('feature-post');
$latest_post_class           = array('post-card');
$category_feature_post_class = array('post-card', 'category-feature-post');
$recipes_post_class          = array('post-card', 'recipes-post');
$category_post_class         = array('post-card', 'category-post', 'p-10');
$single_post_class           = array('custom-single-post');
$diy_easy_fun_post_class     = array('post-card', 'diy-easy-fun-post');
$spotlight_post_class        = array('post-card', 'spotlight');
$blog_post_class             = array('post-card', 'blog-post');
$event_list_post_class       = array('post-card', 'event-listing');

//constant variable
//==================

function themeplate_body_classes($classes)
{
	// Adds a class of group-blog to blogs with more than 1 published author.
	if (is_multi_author()) {
		$classes[] = 'group-blog';
	}
	// Adds a class of hfeed to non-singular pages.
	if (!is_singular()) {
		$classes[] = 'hfeed';
	}
	return $classes;
}

add_filter('body_class', 'themeplate_body_classes');

//custom date format function
function gm_date_format($date)
{
	$date = strtotime($date);
	return date('M d,Y', $date);
}

//date to day name
function gm_date_to_day_name($from_date, $to_date)
{
	$from_date = new DateTime($from_date);
	$to_date   = new DateTime($to_date);
	ob_start();
	for ($date = $from_date; $date <= $to_date; $date->modify('+1 day')) {
		echo $date->format('l') . ", ";
	}
	$output = ob_get_clean();
	$days   = rtrim($output, ', ');
	echo $days . ".";
}

function gm_get_share_link($post_id, $type)
{
	switch ($type) {
		case 'facebook';
			return add_query_arg([
				'u'     => get_the_permalink($post_id),
				'title' => get_the_title($post_id),
			], 'http://www.facebook.com/sharer.php');
			break;
		case 'twitter';
			return add_query_arg([
				'url'  => get_the_permalink($post_id),
				'text' => get_the_title($post_id),
			], 'http://twitter.com/share');
			break;

		case 'linkedin';
			return add_query_arg([
				'url'   => get_the_permalink($post_id),
				'title' => get_the_title($post_id),
			], 'http://www.linkedin.com/shareArticle');
			break;

		case 'reddit';
			return add_query_arg([
				'url'   => get_the_permalink($post_id),
				'title' => get_the_title($post_id),
			], 'http://reddit.com/submit');
			break;

		case 'googleplus';
			return add_query_arg([
				'url' => get_the_permalink($post_id),
			], 'https://plus.google.com/share');
			break;

		case 'tumblr';
			return add_query_arg([
				'url'  => get_the_permalink($post_id),
				'name' => get_the_title($post_id),
			], 'https://www.tumblr.com/share/link');
			break;

		case 'pinterest';
			return add_query_arg([
				'media'       => wp_trim_words(get_the_post_thumbnail($post_id),20),
				'url'         => get_the_permalink($post_id),
				'description' => get_the_title($post_id),
			], 'https://pinterest.com/pin/create/bookmarklet');
			break;

		case 'vk';
			return add_query_arg([
				'url'   => get_the_permalink($post_id),
				'title' => get_the_title($post_id),
			], 'http://vk.com/share.php?');
			break;

		case 'email';
			return add_query_arg([
				'subject' => "I wanted you to see this site&amp",
				'body'    => 'body=Check out this site ' . get_the_permalink($post_id),
			], 'mailto:');
			break;
	}
}

