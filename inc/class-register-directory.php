<?php

class KCP_directoryListing
{

	protected static $instance = null;

	/**
	 * PostTypes constructor.
	 */
	public function __construct()
	{
		add_action('init', array($this, 'register_post_types'));
		add_action('init', array($this, 'register_taxonomies'));
	}

	/**
	 * Register custom post types
	 */
	public function register_post_types()
	{
		register_post_type('past_issues', array(
			'labels' => $this->get_posts_labels('Past issue', __('Past Issues', 'themeplate'), __('Past Issues', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'capability_type' => 'post',
			'rewrite' => true
		));

		register_post_type('printable', array(
			'labels' => $this->get_posts_labels('Printables', __('Printables', 'themeplate'), __('Printables', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'capability_type' => 'post',
			'rewrite' => true
		));


	}

	/**
	 * Register custom taxonomies
	 */
	public function register_taxonomies()
	{
		register_taxonomy('printable_category', array('printable'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Category', 'themeplate'), __('Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => false,
			'rewrite' => array('slug' => 'printable-category'),
		));


	}

	/**
	 * Get all labels from post types
	 */
	protected static function get_posts_labels($menu_name, $singular, $plural)
	{
		$labels = array(
			'name' => $singular,
			'all_items' => sprintf(__("All %s", 'themeplate'), $plural),
			'singular_name' => $singular,
			'add_new' => sprintf(__('New %s', 'themeplate'), $singular),
			'add_new_item' => sprintf(__('Add New %s', 'themeplate'), $singular),
			'edit_item' => sprintf(__('Edit %s', 'themeplate'), $singular),
			'new_item' => sprintf(__('New %s', 'themeplate'), $singular),
			'view_item' => sprintf(__('View %s', 'themeplate'), $singular),
			'search_items' => sprintf(__('Search %s', 'themeplate'), $plural),
			'not_found' => sprintf(__('No %s found', 'themeplate'), $plural),
			'not_found_in_trash' => sprintf(__('No %s found in Trash', 'themeplate'), $plural),
			'parent_item_colon' => sprintf(__('Parent %s:', 'themeplate'), $singular),
			'menu_name' => $menu_name,
		);

		return $labels;
	}

	/**
	 * Get all labels from taxonomies
	 */
	protected static function get_taxonomy_label($menu_name, $singular, $plural)
	{
		$labels = array(
			'name' => sprintf(_x('%s', 'taxonomy general name', 'themeplate'), $plural),
			'singular_name' => sprintf(_x('%s', 'taxonomy singular name', 'themeplate'), $singular),
			'search_items' => sprintf(__('Search %', 'themeplate'), $plural),
			'all_items' => sprintf(__('All %s', 'themeplate'), $plural),
			'parent_item' => sprintf(__('Parent %s', 'themeplate'), $singular),
			'parent_item_colon' => sprintf(__('Parent %s:', 'themeplate'), $singular),
			'edit_item' => sprintf(__('Edit %s', 'themeplate'), $singular),
			'update_item' => sprintf(__('Update %s', 'themeplate'), $singular),
			'add_new_item' => sprintf(__('Add New %s', 'themeplate'), $singular),
			'new_item_name' => sprintf(__('New % Name', 'themeplate'), $singular),
			'menu_name' => __($menu_name, 'themeplate'),
		);

		return $labels;
	}
}

