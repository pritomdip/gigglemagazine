<?php global $author; ?>
<!--author-info-area-->
<div class="author-info-area p-r-120">
	<div class="author-info-inner">
		<div class="author-info-top">
			<?php echo get_avatar($author->user_email, '120', $default = '', $alt = 'author-image', $args = array('class' => array('img-circle'))); ?>
		</div>
		<div class="author-info-bottom">
			<h3 class="title">
				<?php
				$user_url       = get_the_author_meta('user_url', $author->ID);
				$user_twitter   = get_the_author_meta('twitter', $author->ID);
				$user_instagram = get_the_author_meta('instagram', $author->ID);

				if (!empty($author->website)) {
					echo '<a href="' . $author->website . '"> <span class="writer-name">' . $author->display_name . '</span></a>';
				} elseif (!empty($user_url)) {
					echo '<a href="' . $user_url . '"> <span class="writer-name">' . $author->display_name . '</span></a>';
				} else {
					echo '<span class="writer-name">' . $author->display_name . '</span>';
				}
				?>
				<span class="icon"><i class="fa fa-asterisk"></i></span>

				<?php if (!empty($user_twitter)) { ?>
					<span class="separator">#</span>
					<a href="https://twitter.com/<?php echo $user_twitter; ?>"
					   class="twitter-link" target="_blank">
						<?php
						echo $user_twitter;
						?>
					</a>
				<?php }
				if (!empty($user_instagram)) { ?>
					<span class="separator">or</span>
					<a href="https://instagram.com/<?php echo $user_instagram; ?>"
					   class="instagram-link" target="_blank">
						<?php
						echo $user_instagram;
						?>
					</a>
				<?php } ?>
			</h3>
			<p class="desc">
				<?php echo $author->description; ?>
			</p>
		</div>
	</div>
</div><!--author-info-area-->
