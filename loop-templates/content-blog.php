<?php
global $blog_post_class;
?>
<div class="col-md-6 col-sm-6">
	<article id="post-<?php the_ID(); ?>" <?php post_class($blog_post_class); ?>>
		<div class="blog-post-top">
			<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID, 'blog-thumb'); ?></a>
		</div>
		<div class="blog-post-bottom">
			<header class="entry-header">
				<?php the_title(sprintf('<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>
				<?php if ('post' == get_post_type()) : ?>
					<div class="entry-meta">
						<span class="meta-author">By <?php echo get_the_author(); ?></span>
						<span class="icon">|</span>
						<span class="meta-date"><?php echo get_the_date(); ?></span>
						<span class="icon">|</span>
						<span class="category-meta">
							<?php
							$post_categories = get_the_terms($post->ID, 'category');
							if (!empty($post_categories)) {
								$post_categories = wp_list_pluck($post_categories, 'name', 'term_id');
								$category_list   = array();
								foreach ($post_categories as $key => $post_category) {
									$category_list[] = '<a href="' . get_category_link($key) . '">' . $post_category . '</a> ';
								}
								echo implode(', ', $category_list);
							} ?>
					   </span>
					</div><!-- .entry-meta -->
				<?php endif; ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
				$more_tag = '<p><a class="btn read-more" href="' . get_the_permalink() . '">Read More <span class="fa fa-angle-right"></span></a></p>';
				$content  = get_the_content();
				echo wp_trim_words($content, 55, $more_tag);
				?>
			</div><!-- .entry-content -->
		</div>
	</article><!-- #post-## -->
</div>
