<?php
global $feature_post_class;
$subtitle = get_post_meta($post->ID, 'subtitle', true);
?>
<div class="feature-slider-item">
	<div <?php post_class($feature_post_class) ?>>
		<div class="feature-post-top">
			<?php
			if (has_post_thumbnail()) { ?>
				<a href="<?php echo get_the_permalink($post); ?>">
					<?php if (has_post_thumbnail()) {
						echo get_the_post_thumbnail($post->ID, 'feature-post-thumb');
					} else { ?>
						<img class="image-responsive"
							 src="<?php echo bloginfo('template_directory') ?>/assets/images/noimg.jpg"
							 alt="thumbnail image">
					<?php } ?>
				</a>
			<?php } ?>
		</div>
		<div class="feature-post-bottom">

			<div class="meta-category">
				<?php
				$terms = get_the_terms($post->ID, 'category');
				if (!empty($terms)) {
					$term_link = get_term_link($terms[0]->term_id);
					echo '<a class="category-name" href="' . $term_link . '">' . $terms[0]->name . '</a>';
				}
				/*foreach ($terms as $term) {
					$term_link = get_term_link($term);
					echo '<a class="category-name" href="' . $term_link . '">' . $term->name . '</a>';
				}*/
				?>
			</div>
			<h3 class="title">
				<a href="<?php echo get_the_permalink($post); ?>"><?php echo wp_trim_words(get_the_title($post), 5); ?></a>
			</h3>

			<?php if (!empty($subtitle)) echo '<h4 class="sub-title">' . $subtitle . '</h4>' ?>

			<!--<div class="feature-post-content">
				<?php
			/*				$content = $post->post_content;
                            echo wp_trim_words($content, 15);
                            */ ?>
			</div>-->
		</div>
	</div>
</div>
