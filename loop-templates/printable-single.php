<?php
/**
 * @package themeplate
 */
global $single_post_class;
$pdf_upload = get_post_meta($post->ID, '_pdf_upload', true);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($single_post_class); ?>>

	<?php echo get_the_post_thumbnail($post->ID, 'large'); ?>

	<div class="entry-content">
		<div class="row">

			<div class="col-md-6">
				<h3>Project Description</h3>
				<p>Download high resolution PDF below</p>
				<a href="<?php echo $pdf_upload ?>" class="btn btn-default custom-btn">Download</a>
			</div>

			<div class="col-md-6">
				<div class="pull-right">
					<h3>Project Details</h3>
					<div class="project-info-box">
						<h4>Categories:</h4>
						<div class="project-terms">
							<?php
							$post_categories = get_the_terms($post->ID, 'printable_category');
							if (!empty($post_categories)) {
								$post_categories = wp_list_pluck($post_categories, 'name', 'term_id');
								$category_list   = array();
								foreach ($post_categories as $key => $post_category) {
									$category_list[] = '<a href="' . get_category_link($key) . '">' . $post_category . '</a> ';
								}
								echo implode(', ', $category_list);
							} ?>
						</div>
					</div>
				</div>
			</div><!--col-md-6-->

		</div>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
