<?php
global $spotlight_post_class;
$more_btn = '<a href="' . get_the_permalink($post) . '" class="more-btn">More <span><i
						class="fa fa-angle-right"></i></span></a>'
?>
<div <?php post_class($spotlight_post_class); ?>>
	<?php if (has_post_thumbnail($post)) { ?>
		<div class="post-card-top">
			<a href="<?php echo get_the_permalink($post) ?>">
				<?php if (has_post_thumbnail()) {
					echo get_the_post_thumbnail($post->ID, 'spotlight-thumb');
				} else { ?>
					<img class="image-responsive"
						 src="<?php echo bloginfo('template_directory') ?>/assets/images/noimg.jpg"
						 alt="thumbnail image">
				<?php } ?>
			</a>
		</div>
	<?php } ?>
	<div class="post-card-bottom">
		<h3 class="post-title"><a href="<?php echo get_the_permalink($post); ?>"><?php echo get_the_title($post); ?></a></h3>
		<?php echo wp_trim_words(get_the_content($post), 50, $more_btn) ?>
	</div>
</div><!--post-card-->
