<?php
global $category_post_class;
?>
<div class="col-md-4 col-sm-6 col-xs-6">
	<div <?php post_class($category_post_class) ?>>
		<div class="post-card-top">
			<a class="post-thumbnail-link" href="<?php echo get_the_permalink($post) ?>">
				<?php if (has_post_thumbnail()) {
					echo get_the_post_thumbnail($post->ID, 'thumbnail');
				} else { ?>
					<img class="image-responsive"
						 src="<?php echo bloginfo('template_directory') ?>/assets/images/noimg.jpg"
						 alt="thumbnail image">
				<?php } ?>
			</a>
		</div>
		<div class="post-card-bottom">
			<h3 class="title"><a href="<?php echo get_the_permalink($post) ?>"><?php echo get_the_title($post) ?></a>
			</h3>
		</div>
	</div>
</div>
