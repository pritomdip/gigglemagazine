<?php
$author_name      = get_post_meta($post->ID, 'author', true);
$event_start_date = get_post_meta($post->ID, 'startdate', true);
$event_end_date   = get_post_meta($post->ID, 'enddate', true);
$event_start_time = get_post_meta($post->ID, 'starttime', true);
$event_location   = get_post_meta($post->ID, 'location', true);

global $event_list_post_class;
?>

<div class="col-md-12 padding-lr5">
	<div <?php post_class($event_list_post_class); ?>>


		<div class="event-header">
			<h2 class="event-title"><a
					href="<?php echo get_the_permalink($post); ?>"><?php echo get_the_title($post) ?></a></h2>
			<div class="clearfix">
				<p class="event-location pull-right"><?php echo $event_location ?></p>
				<p class="event-date pull-left"><?php if (!empty($event_start_date)) echo gm_date_format($event_start_date) ?>
					&nbsp;-&nbsp; <?php if (!empty($event_end_date)) echo gm_date_format($event_end_date) ?>
					&nbsp;@&nbsp;<?php echo $event_start_time ?></p>
			</div>
		</div>


		<div class="event-body">
			<div class="calendar-list-data">
				<p class="recurrence">
					<img src="<?php echo bloginfo("template_directory") ?>/assets/images/recur.png" height="26"
						 width="31">
					This event occurs weekly, on <?php if (!empty($event_start_date) && !empty($event_end_date)) {
						gm_date_to_day_name($event_start_date, $event_end_date);
					} ?>
				</p>

				<div class="event-desc clearfix">
					<div class="calendar-list-image">
						<a href="<?php echo get_the_permalink($post); ?>">
							<?php echo get_the_post_thumbnail($post->ID, 'thumbnail'); ?>
						</a>
					</div>

					<?php
					$content = get_the_content();
					echo wp_trim_words($content, 80, '');
					?>

					<p class="event-detail">
						<a href="<?php echo get_the_permalink($post); ?>"><strong>View vent details</strong></a>
					</p>

					<div class="event-categories">
						<?php
						$tags = get_the_terms(get_the_ID(), 'event_category');
						if (!empty($tags)) {
							$tag_names = wp_list_pluck($tags, 'name', 'term_id');
							foreach ($tag_names as $key => $tag_name) {
								echo '<a href="' . get_tag_link($key) . '">' . $tag_name . '</a>';
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
