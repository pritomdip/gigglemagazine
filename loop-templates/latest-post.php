<?php
global $latest_post_class;

//add sponsored class
$sponsored = !empty(get_post_meta($post->ID, '_sponsored', true)) ? 'sponsored' : '';
if (!empty($sponsored)) {
	array_push($latest_post_class, $sponsored);
} else {
	if (($key = array_search('sponsored', $latest_post_class)) !== false) {
		unset($latest_post_class[$key]);
	}
}
?>
<div class="col-md-4 col-sm-6 col-xs-6">
	<div <?php post_class($latest_post_class) ?>>
		<div class="post-card-top">
			<a href="<?php echo get_the_permalink($post) ?>">
				<?php if (has_post_thumbnail()) {
					echo get_the_post_thumbnail($post->ID, 'thumbnail');
				} else { ?>
					<img class="image-responsive"
						 src="<?php echo bloginfo('template_directory') ?>/assets/images/noimg.jpg"
						 alt="thumbnail image">
				<?php } ?>
			</a>
		</div>
		<div class="post-card-bottom">
			<div class="meta-category">
				<?php
				$terms = get_the_terms($post->ID, 'category');
				if (!empty($terms)) {
					$term_link = get_term_link($terms[0]->term_id);
					echo '<a class="category-name" href="' . $term_link . '">' . $terms[0]->name . '</a>';
				}
				/*foreach ($terms as $term) {
					$term_link = get_term_link($term);
					echo '<a class="category-name" href="' . $term_link . '">' . $term->name . '</a>';
				}*/
				?>
			</div>
			<h3 class="title"><a href="<?php echo get_the_permalink($post) ?>"><?php echo get_the_title($post) ?></a>
			</h3>
			<?php if (!empty($sponsored)) { ?>
				<p class="sp-text"><?php echo $sponsored; ?></p>
			<?php } ?>
		</div>
	</div>
</div>
