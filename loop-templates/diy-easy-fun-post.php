<?php
global $diy_easy_fun_post_class;
?>

<div <?php post_class($diy_easy_fun_post_class) ?>>
	<div class="side-by-side">
		<div class="post-card-top">
			<a href="<?php echo get_the_permalink($post) ?>">
				<?php if (has_post_thumbnail()) {
					echo get_the_post_thumbnail($post->ID, 'thumbnail');
				} else { ?>
					<img class="image-responsive"
						 src="<?php echo bloginfo('template_directory') ?>/assets/images/noimg.jpg"
						 alt="thumbnail image">
				<?php } ?>
			</a>
		</div>
		<div class="post-card-bottom">
			<div class="meta-category">
				<?php
				$terms = get_the_terms($post->ID, 'category');
				if (!empty($terms)) {
					$term_link = get_term_link($terms[0]->term_id);
					echo '<a class="category-name" href="' . $term_link . '">' . $terms[0]->name . '</a>';
				}
				/*foreach ($terms as $term) {
					$term_link = get_term_link($term);
					echo '<a class="category-name" href="' . $term_link . '">' . $term->name . '</a>';
				}*/
				?>
			</div>
			<h3 class="title"><a href="<?php echo get_the_permalink($post) ?>"><?php echo get_the_title($post) ?></a>
			</h3>
		</div>
	</div>
</div>

