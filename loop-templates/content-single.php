<?php
/**
 * @package themeplate
 */
global $single_post_class;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($single_post_class); ?>>

	<header class="entry-header">

		<?php if ($post->post_type != 'pro_event') { ?>
			<div class="entry-category">
				<?php
				$category = get_the_category($post->ID);
				if (!empty($category)) {
					$category_name = $category[0]->cat_name;
					$category_id   = $category[0]->term_id;
				}
				?>
				<a href="<?php echo get_category_link($category_id); ?>"
				   class="category-name"><?php echo $category_name; ?></a>
			</div>
		<?php } ?>

		<?php the_title('<h2 class="entry-title">', '</h2>'); ?>

		<?php if ($post->post_type != 'pro_event') { ?>
			<div class="entry-meta">
				<span class="meta-author">By <?php echo get_the_author(); ?></span>
				<span class="icon">|</span>
				<span class="meta-date"><?php echo get_the_date(); ?></span>
			</div>
		<?php } ?>
	</header>

	<?php if ($post->post_type != 'pro_event') { ?>
		<?php echo get_the_post_thumbnail($post->ID, 'large'); ?>
	<?php } ?>

	<div class="entry-content">
		<?php the_content(); ?>
	</div>

</article>
